package org.y3.jrun.storage.file;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.y3.jrun.model.Model;
import org.y3.jrun.model.database.DatabaseInformation;

import junit.framework.TestCase;

/**
 * Copyright: 2011 - 2018
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class TestDatabaseInformationExchanger extends TestCase {
	
	private static Logger log = LogManager.getLogger();

	private String toLocation = System.getProperty("user.home") + "/testDatabaseInformationExchanger";
	private DatabaseInformation model;
	private DatabaseInformationExchanger exchanger;
	private DatabaseInformation[] models;
	
	@Before
	protected void setUp() throws Exception {
		super.setUp();
		model = new DatabaseInformation();
		model.setHomeLocation(toLocation);
		log.debug("Using this file: " + toLocation);
		models = new DatabaseInformation[]{model};
		exchanger = new DatabaseInformationExchanger();
	}
	
	@Test
	public void testExportModels_noModels() {
		boolean expected = false;
		boolean actual = false;
		try {
			actual = exchanger.exportModels(null, toLocation);
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertEquals(expected, actual);
	}
	
	@Test
	public void testExportModels_oneModel() {
		boolean expected = true;
		boolean actual = false;
		try {
			actual = exchanger.exportModels(models, toLocation);
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertEquals(expected, actual);
	}
	
	@Test
	public void testImportModels() {
		String expected = toLocation;
		String actual = null;
		Model[] importModels = null;
		try {
                    //prepare
                    boolean preparationPositive = exchanger.exportModels(models, toLocation);
                    //test
			importModels = (Model[]) exchanger.importModels(toLocation);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (importModels != null && importModels.length > 0) {
			Model model0 = importModels[0];
			if (model0 instanceof DatabaseInformation) {
				actual = ((DatabaseInformation) model0).getHomeLocation();				
			}
		}
		assertEquals(expected, actual);
	}

}
