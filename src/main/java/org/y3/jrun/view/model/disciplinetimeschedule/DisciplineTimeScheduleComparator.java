package org.y3.jrun.view.model.disciplinetimeschedule;

import java.util.Comparator;
import java.util.Date;

import org.y3.jrun.model.disciplinetimeschedule.DisciplineTimeSchedule;

/**
 * @author Ryppo
 * 
 */
public class DisciplineTimeScheduleComparator implements
	Comparator<DisciplineTimeSchedule> {

    @Override
    public int compare(DisciplineTimeSchedule dts1, DisciplineTimeSchedule dts2) {
	if (dts1 == null && dts2 == null) {
	    return 0;
	} else if (dts1 == null) {
	    return 1;
	} else if (dts2 == null) {
	    return -1;
	} else {
	    Date d1 = dts1.getEndOfTimeSchedule();
	    Date d2 = dts1.getEndOfTimeSchedule();
	    if (d1 == null && d2 == null) {
		return 0;
	    } else if (d1 == null) {
		return 1;
	    } else if (d2 == null) {
		return -1;
	    } else {
		return d1.compareTo(d2);
	    }
	}
    }

}
