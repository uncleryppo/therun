package org.y3.jrun.view.model.competition;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.time.DateUtils;
import org.y3.jrun.control.ApplicationController;
import org.y3.jrun.control.KeywordsDictionary;
import org.y3.jrun.control.Utils;
import org.y3.jrun.model.Model;
import org.y3.jrun.model.ageclass.AgeClass;
import org.y3.jrun.model.ageclassesdefinition.AgeClassesDefinition;
import org.y3.jrun.model.competition.Competition;
import org.y3.jrun.model.contact.Contact;
import org.y3.jrun.model.discipline.Discipline;
import org.y3.jrun.model.discipline.DisciplineListModel;
import org.y3.jrun.model.participation.Participation;
import org.y3.jrun.model.participation.ParticipationDuration;
import org.y3.jrun.model.participation.ParticipationListModel;
import org.y3.jrun.model.participation.RichParticipation;
import org.y3.jrun.model.participation.RichParticipationReportable;
import org.y3.jrun.model.participation.RichParticipationsTableModel;
import org.y3.jrun.model.report.Report;
import org.y3.jrun.model.team.TeamRankingReportable;
import org.y3.jrun.view.ApplicationFrame;
import org.y3.jrun.view.gfx.IconDictionary;
import org.y3.jrun.view.gfx.JLabelRenderer;
import org.y3.jrun.view.gfx.UIHelper;
import org.y3.jrun.view.i18n.Messages;
import org.y3.jrun.view.model.discipline.DisciplineComparator;
import org.y3.jrun.view.model.discipline.DisciplineListCellRenderer;
import org.y3.jrun.view.model.discipline.ParticipationsTable;
import org.y3.jrun.view.model.participation.ParticipationDurationEditor;
import org.y3.jrun.view.model.participation.ParticipationDurationRenderer;
import org.y3.jrun.view.model.participation.ParticipationFormDialog;
import org.y3.jrun.view.reporting.ReportFactory;
import org.y3.jrun.view.reporting.ReportingController;

/**
 * Copyright: Since 2011
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class CompetitionGridPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private ApplicationFrame appFrame;
	private ApplicationController controller;
	private Competition competition;
	private JPanel panel_toolbarRow1, panel_toolbarRow2, jp_toolbars;
	private ParticipationsTable table_participations;
	private JList<Discipline> list_disciplines;
	private JTextField textfield_search;

	private JTextField label_countParticipations;

	private enum RANK_TYPE {
		RANK, AGECLASS_RANK, GENDER_RANK, GENDER_AGECLASS_RANK
	};

	private JSplitPane splitPane;

	public CompetitionGridPanel(Competition _competition, ApplicationController _controller,
			ApplicationFrame _appFrame) {
		appFrame = _appFrame;
		controller = _controller;
		competition = _competition;
		init();
		bindData();
	}

	private void init() {
		setLayout(new BorderLayout(0, 0));
		splitPane = new JSplitPane();
		add(splitPane, BorderLayout.CENTER);

		JPanel jp_leftSplit = new JPanel(new BorderLayout());
		splitPane.setLeftComponent(jp_leftSplit);

		JPanel jp_search = new JPanel(new BorderLayout());
		textfield_search = new JTextField();
		textfield_search.addKeyListener(getKeyListenerForActionSearchModel());
		textfield_search.addKeyListener(getKeyListenerForParticipationsActions());
		jp_search.add(textfield_search, BorderLayout.CENTER);
		JButton jb_search = new JButton(IconDictionary.getImageIcon(IconDictionary.SEARCH));
		jb_search.setToolTipText(Messages.getString(Messages.SEARCH));
		jb_search.addActionListener(getActionListenerForActionSearchModel());
		jp_search.add(jb_search, BorderLayout.EAST);
		jp_leftSplit.add(jp_search, BorderLayout.NORTH);

		list_disciplines = new JList<>();
		list_disciplines.setCellRenderer(new DisciplineListCellRenderer(appFrame, controller, competition));
		list_disciplines.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				UIHelper.startWaiting(appFrame);
				Discipline[] disciplines = getSelectedDisciplines();
				if (disciplines != null && disciplines.length > 0) {
					try {
						RichParticipationsTableModel tModel = controller.getParticipationsTableForDisciplines(
								competition, disciplines, appFrame, null, false, false);
						table_participations.setModel(tModel);
						tModel.addTableModelListener(new TableModelListener() {
							@Override
							public void tableChanged(TableModelEvent e) {
								table_participations.repaint();
							}
						});
						int count = 0;
						if (tModel != null) {
							count = tModel.getRowCount();
							for (int colNo = 0; colNo < tModel.getColumnCount(); colNo++) {
								table_participations.getColumnModel().getColumn(colNo)
										.setPreferredWidth(tModel.getColumnWidth(colNo));
							}
						}
						String text = Integer.toString(count) + " ";
						if (count == 1) {
							text += Messages.getString(Messages.PARTICIPATION);
						} else {
							text += Messages.getString(Messages.PARTICIPATIONS);
						}
						label_countParticipations.setText(text);
					} catch (Exception e1) {
						appFrame.showUserMessage(e1, disciplines[0]);
					}
				} else {
					table_participations.setModel(new DefaultTableModel());
				}
				UIHelper.stopWaiting(appFrame);
			}
		});
		list_disciplines.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent arg0) {
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
			}

			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_UP) {
					ListModel<Discipline> modelList = list_disciplines.getModel();
					if (modelList == null || (modelList != null
							&& (list_disciplines.getSelectedIndex() == 0 || modelList.getSize() == 0))) {
						focusSearchField();
					}
				}
			}
		});
		jp_leftSplit.add(list_disciplines, BorderLayout.CENTER);

		table_participations = new ParticipationsTable();
		table_participations.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table_participations.setDefaultRenderer(ParticipationDuration.class, new ParticipationDurationRenderer());
		table_participations.setDefaultRenderer(JLabel.class, new JLabelRenderer());
		table_participations.setDefaultEditor(ParticipationDuration.class,
				new ParticipationDurationEditor(controller, appFrame));
		table_participations.setRowHeight(new JFormattedTextField("#").getPreferredSize().height);
		table_participations.addKeyListener(getKeyListenerForParticipationsActions());
		// list_disciplines.addKeyListener(getKeyListenerForParticipationsActions());
		// textfield_search.addKeyListener(getKeyListenerForParticipationsActions());

		JPanel panel = new JPanel(new BorderLayout());
		panel.add(new JScrollPane(table_participations), BorderLayout.CENTER);
		label_countParticipations = new JTextField();
		panel.add(label_countParticipations, BorderLayout.SOUTH);
		splitPane.setRightComponent(panel);

		jp_toolbars = new JPanel(new BorderLayout());
		jp_toolbars.add(getToolbarRow1(), BorderLayout.NORTH);
		jp_toolbars.add(getToolbarRow2(), BorderLayout.SOUTH);
		add(jp_toolbars, BorderLayout.NORTH);

	}

	private void addFeatureButton(ImageIcon functionIcon, String functionName, ActionListener functionActionListener,
								  JPanel toolbar) {
		JButton button = new JButton(functionIcon);
		button.setToolTipText(functionName);
		button.addActionListener(functionActionListener);
		toolbar.add(button);
	}

	private JPanel getToolbarRow1() {
		if (panel_toolbarRow1 == null) {
			panel_toolbarRow1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		}
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.REFRESH), Messages.getString(Messages.REFRESH),
				getActionListenerForActionRefresh(), panel_toolbarRow1);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.REPORT), Messages.getString(Messages.REPORT),
				getActionListenerForActionReportDisciplineStartGrid(), panel_toolbarRow1);

		panel_toolbarRow1.add(new JSeparator(JSeparator.VERTICAL));
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.DONATION), Messages.getString(Messages.DONATION),
				getActionListenerForActionReportDonationsReport(), panel_toolbarRow1);
		panel_toolbarRow1.add(new JSeparator(JSeparator.VERTICAL), panel_toolbarRow1);

		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.REPORT),
				Messages.getString(Messages.PARTICIPATIONS_REPORT),
				getActionListenerForActionCreateParticipationsReport(), panel_toolbarRow1);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.REPORT),
				Messages.getString(Messages.KIDS_GRID_REPORT),
				getActionListenerForActionReportKidsStartGrid(), panel_toolbarRow1);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.REPORT),
				Messages.getString(Messages.GENDER_REPORTS),
				getActionListenerForActionReportDisciplineGenderStartGrid(), panel_toolbarRow1);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.REPORT),
				Messages.getString(Messages.GENDER_AGECLASS_REPORTS),
				getActionListenerForActionReportDisciplineGenderAgeClassStartGrid(), panel_toolbarRow1);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.REPORT),
				Messages.getString(Messages.TEAM_RANKING_REPORT), getActionListenerForActionReportTeamRanking()
		, panel_toolbarRow1);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.REPORT_ALL),
				Messages.getString(Messages.REPORT_ALL_START_GRIDS),
				getActionListenerForActionCreateAllDisciplineStartGridReports(), panel_toolbarRow1);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.CERTIFICATION_MF_AC),
				Messages.getString(Messages.REPORT_CERTIFICATION_MF_AC),
				getActionListenerForActionReportCertification(), panel_toolbarRow1);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.CERTIFICATION_MF),
				Messages.getString(Messages.REPORT_CERTIFICATION_MF),
				getActionListenerForActionReportCertificationMf(), panel_toolbarRow1);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.CERTIFICATION_KIDS),
				Messages.getString(Messages.REPORT_CERTIFICATION_KIDS),
				getActionListenerForActionReportCertificationKids(), panel_toolbarRow1);

		panel_toolbarRow1.add(new JSeparator(JSeparator.VERTICAL));

		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.CONFIRMATION),
				Messages.getString(Messages.REPORT_PARTICIPATION_CONFIRMATION),
				getActionListenerForActionReportParticipationConfirmation(), panel_toolbarRow1);

		panel_toolbarRow1.add(new JSeparator(JSeparator.VERTICAL));

		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.NEW), Messages.getString(Messages.NEW),
				getActionListenerForActionAddParticipant(), panel_toolbarRow1);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.EDIT), Messages.getString(Messages.EDIT) + " [F2]",
				getActionListenerForActionEditParticipation(), panel_toolbarRow1);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.REMOVE), Messages.getString(Messages.REMOVE),
				getActionListenerForActionRemoveParticipation(), panel_toolbarRow1);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.REMOVE_ALL_FROM_LIST),
				Messages.getString(Messages.REMOVE_ALL_PARTICIPATIONS_FROM_DISCIPLINE),
				getActionListenerForActionRemoveAllParticipantsFromDiscipline(), panel_toolbarRow1);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.REMOVE_ALL_FROM_COMPETITION),
				Messages.getString(Messages.REMOVE_ALL_PARTICIPATIONS_FROM_COMPETITION),
				getActionListenerForActionRemoveAllParticipantsFromCompetition(), panel_toolbarRow1);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.IMPORT),
				Messages.getString(Messages.IMPORT_PARTICIPATION_DATA),
				getActionListenerForActionImportParticipationData(), panel_toolbarRow1);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.EXPORT),
				Messages.getString(Messages.EXPORT_PARTICIPATION_DATA),
				getActionListenerForActionExportParticipationData(), panel_toolbarRow1);

		return panel_toolbarRow1;
	}

	private JPanel getToolbarRow2() {
		if (panel_toolbarRow2 == null) {
			panel_toolbarRow2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		}
		panel_toolbarRow2.add(new JSeparator(JSeparator.VERTICAL));

		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.RESET_DISCIPLINE_GRID),
				Messages.getString(Messages.RESET_DISCIPLINE_GRID), getActionListenerForActionResetDisciplineGrid()
				, panel_toolbarRow2);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.RESET_DISCIPLINE_RANKINGS),
				Messages.getString(Messages.RESET_DISCIPLINE_RANKINGS),
				getActionListenerForActionResetDisciplineRankings(), panel_toolbarRow2);

		panel_toolbarRow2.add(new JSeparator(JSeparator.VERTICAL));

		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.REMOVE),
				Messages.getString(Messages.CLEAR_ALL_RANKINGS), getActionListenerForActionClearAllRankings()
				, panel_toolbarRow2);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.RANKING),
				Messages.getString(Messages.CALCULATE_RANKING), getActionListenerForActionCalculateRanking()
				, panel_toolbarRow2);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.RANKING),
				Messages.getString(Messages.CALCULATE_GENDER_RANKING),
				getActionListenerForActionCalculateGenderRanking(), panel_toolbarRow2);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.RANKING),
				Messages.getString(Messages.CALCULATE_AGECLASS_RANKING),
				getActionListenerForActionCalculateAgeClassRanking(), panel_toolbarRow2);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.RANKING),
				Messages.getString(Messages.CALCULATE_GENDER_AGECLASS_RANKING),
				getActionListenerForActionCalculateGenderAgeClassRanking(), panel_toolbarRow2);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.RANKING_ALL),
				Messages.getString(Messages.CALCULATE_ALL_RANKINGS), getActionListenerForActionCalculateAllRankings()
				, panel_toolbarRow2);

		panel_toolbarRow2.add(new JSeparator(JSeparator.VERTICAL));

		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.CONTACT), Messages.getString(Messages.SHOW_CONTACT),
				getActionListenerForActionShowContact(), panel_toolbarRow2);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.TEAM), Messages.getString(Messages.SHOW_TEAM),
				getActionlistenerForActionShowTeam(), panel_toolbarRow2);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.PARTICIPATION),
				Messages.getString(Messages.SHOW_PARTICIPATION), getActionListenerForActionShowParticipation()
				, panel_toolbarRow2);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.PARTICIPATIONS),
				Messages.getString(Messages.SHOW_PARTICIPATIONS), getActionListenerForActionShowParticipations()
				, panel_toolbarRow2);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.DISCIPLINE),
				Messages.getString(Messages.SHOW_DISCIPLINE), getActionListenerForActionShowDiscipline()
				, panel_toolbarRow2);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.AGECLASSES_DEFINITION),
				Messages.getString(Messages.SHOW_AGECLASSES_DEFINITION),
				getActionListenerForActionShowAgeClassesDefinition(), panel_toolbarRow2);
		addFeatureButton(IconDictionary.getImageIcon(IconDictionary.COMPETITION),
				Messages.getString(Messages.SHOW_COMPETITION), getActionListenerForActionShowCompetition()
				, panel_toolbarRow2);

		return panel_toolbarRow2;
	}

	@SuppressWarnings("unchecked")
	public void bindData() {
		UIHelper.startWaiting(appFrame);
		if (competition != null) {
			try {
				Discipline[] selectedDisciplines = getSelectedDisciplines();
				Participation selectedParticipation = table_participations.getSelectedParticipation();
				DisciplineListModel disciplinesByCompetitionId = controller
						.getDisciplinesByCompetitionId(competition.getId());
				DisciplineComparator disciplineComparator = new DisciplineComparator();
				disciplineComparator.COMPARE_MODE = DisciplineComparator.compareBy.START_TIME;

				list_disciplines.setModel(disciplinesByCompetitionId);
				((DisciplineListModel) list_disciplines.getModel()).sort(disciplineComparator);
				// update UI
				list_disciplines.repaint();
				setSelectedDisciplines(selectedDisciplines);
				setSelectedParticipation(selectedParticipation);
				optimizeSplit();
			} catch (Exception e) {
				appFrame.showUserMessage(e, competition);
			}
		}
		UIHelper.stopWaiting(appFrame);
	}

	public int getCompetitionId() {
		int id = 0;
		if (competition != null) {
			id = competition.getId();
		}
		return id;
	}

	public void setSelectedParticipation(Participation participation) {
		ListModel<Discipline> listModel = list_disciplines.getModel();
		if (listModel instanceof DisciplineListModel && participation != null) {
			Discipline[] disciplines = ((DisciplineListModel) listModel).getModel();
			boolean participationIsSelected = false;
			for (Discipline discipline : disciplines) {
				if (discipline.getId() == participation.getDisciplineId()) {
					list_disciplines.setSelectedValue(discipline, true);
					if (table_participations.getModel() instanceof RichParticipationsTableModel) {
						RichParticipationsTableModel richParticipationsTableModel = (RichParticipationsTableModel) table_participations
								.getModel();
						for (int i = 0; i < richParticipationsTableModel.getRowCount(); i++) {
							RichParticipation richParticipation = richParticipationsTableModel.getParticipationAtRow(i);
							if (richParticipation != null) {
								if (richParticipation.getId() == participation.getId()) {
									table_participations.getSelectionModel().setSelectionInterval(i, i);
									participationIsSelected = true;
									break;
								}
							}
						}
						if (participationIsSelected) {
							break;
						}
					}
				}
			}
		}
	}

	private ActionListener getActionListenerForActionCalculateRanking() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				UIHelper.startWaiting(appFrame);
				calculateRanking();
				UIHelper.stopWaiting(appFrame);
			}
		};
	}

	public void calculateRanking() {
		calculateRanking(RANK_TYPE.RANK);
	}

	private ActionListener getActionListenerForActionCalculateAgeClassRanking() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				UIHelper.startWaiting(appFrame);
				calculateAgeClassRanking();
				UIHelper.stopWaiting(appFrame);
			}
		};
	}

	public void calculateAgeClassRanking() {
		calculateRanking(RANK_TYPE.AGECLASS_RANK);
	}

	private ActionListener getActionListenerForActionCalculateGenderAgeClassRanking() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				UIHelper.startWaiting(appFrame);
				calculateGenderAgeClassRanking();
				UIHelper.stopWaiting(appFrame);
			}
		};
	}

	public void calculateGenderAgeClassRanking() {
		calculateRanking(RANK_TYPE.GENDER_AGECLASS_RANK);
	}

	private ActionListener getActionListenerForActionCalculateGenderRanking() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				UIHelper.startWaiting(appFrame);
				calculateGenderRanking();
				UIHelper.stopWaiting(appFrame);
			}
		};
	}
	
	private ActionListener getActionListenerForActionClearAllRankings() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				UIHelper.startWaiting(appFrame);
				clearAllRankings();
				UIHelper.stopWaiting(appFrame);
			}
		};
	}
	
	public void clearAllRankings() {
		if (table_participations != null && table_participations.getParticipationsTableModel() != null) {
			RichParticipation[] participations = table_participations.getParticipationsTableModel()
					.getAllParticipations();
			try {
				controller.clearAllRankings(participations, true);
			} catch (Exception e1) {
				appFrame.showUserMessage(e1, null);
			}
			bindData();
		}
	}

	public void calculateGenderRanking() {
		calculateRanking(RANK_TYPE.GENDER_RANK);
	}

	private ActionListener getActionListenerForActionCalculateAllRankings() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (UIHelper.showSureRequest(Messages.getString(Messages.CALCULATE_ALL_RANKINGS), appFrame)) {
					UIHelper.startWaiting(appFrame);
					calculateRanking();
					calculateGenderRanking();
					calculateAgeClassRanking();
					calculateGenderAgeClassRanking();
					UIHelper.stopWaiting(appFrame);
				}
			}
		};
	}

	private void calculateRanking(RANK_TYPE rankType) {
		if (table_participations != null && table_participations.getParticipationsTableModel() != null) {
			RichParticipation[] participations = table_participations.getParticipationsTableModel()
					.getAllParticipations();
			try {
				if (rankType == RANK_TYPE.RANK) {
					controller.calculateRanking(participations, true);
				} else if (rankType == RANK_TYPE.AGECLASS_RANK) {
					controller.calculateAgeClassesRankings(participations, true);
				} else if (rankType == RANK_TYPE.GENDER_RANK) {
					controller.calculateGenderRankings(participations, true);
				} else if (rankType == RANK_TYPE.GENDER_AGECLASS_RANK) {
					controller.calculateGenderAgeClassRankings(participations, true);
				}
			} catch (Exception e1) {
				appFrame.showUserMessage(e1, null);
			}
			bindData();
		}
	}

	private ActionListener getActionListenerForActionShowContact() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Participation model = table_participations.getSelectedParticipation();
				if (model != null) {
					appFrame.showContactById(model.getContactId());
				} else {
					appFrame.showUserMessage(JOptionPane.WARNING_MESSAGE,
							Messages.getString(Messages.NO_PARTICIPATION_SELECTED_SELECT_ONE_FIRST), null);
				}
			}
		};
	}

	private ActionListener getActionlistenerForActionShowTeam() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Participation model = table_participations.getSelectedParticipation();
				if (model != null) {
					appFrame.showTeamById(model.getTeamId());
				} else {
					appFrame.showUserMessage(JOptionPane.WARNING_MESSAGE,
							Messages.getString(Messages.NO_PARTICIPATION_SELECTED_SELECT_ONE_FIRST), null);
				}
			}
		};
	}

	private ActionListener getActionListenerForActionShowParticipations() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Participation model = table_participations.getSelectedParticipation();
				if (model != null) {
					appFrame.showParticipationsByContactId(model.getContactId());
				} else {
					appFrame.showUserMessage(JOptionPane.WARNING_MESSAGE,
							Messages.getString(Messages.NO_PARTICIPATION_SELECTED_SELECT_ONE_FIRST), null);
				}
			}
		};
	}

	private ActionListener getActionListenerForActionShowParticipation() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Participation model = table_participations.getSelectedParticipation();
				if (model != null) {
					appFrame.showParticipationById(model.getId());
				} else {
					appFrame.showUserMessage(JOptionPane.WARNING_MESSAGE,
							Messages.getString(Messages.NO_PARTICIPATION_SELECTED_SELECT_ONE_FIRST), null);
				}
			}
		};
	}

	private ActionListener getActionListenerForActionShowDiscipline() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int disciplineId = 0;
				Participation model = table_participations.getSelectedParticipation();
				if (model != null) {
					disciplineId = model.getDisciplineId();
				} else {
					Object disObject = list_disciplines.getSelectedValue();
					if (disObject != null && disObject instanceof Discipline) {
						Discipline discipline = (Discipline) disObject;
						disciplineId = discipline.getId();
					}
				}
				if (disciplineId > 0) {
					appFrame.showDisciplineById(disciplineId);
				} else {
					appFrame.showUserMessage(JOptionPane.WARNING_MESSAGE,
							Messages.getString(Messages.NO_PARTICIPATION_SELECTED_SELECT_ONE_FIRST), null);
				}
			}
		};
	}

	private ActionListener getActionListenerForActionShowCompetition() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (competition != null) {
					appFrame.showCompetitionById(competition.getId());
				}
			}
		};
	}

	private ActionListener getActionListenerForActionShowAgeClassesDefinition() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				UIHelper.startWaiting(appFrame);
				try {
					if (competition != null) {
						AgeClassesDefinition acd = controller.getAgeClassesDefinitionByCompetition(competition);
						int acdId = 0;
						if (acd != null) {
							acdId = acd.getId();
						}
						appFrame.showAgeClassesDefinitionById(acdId);
					}
				} catch (Exception ex) {
					appFrame.showUserMessage(ex, competition);
				}
				UIHelper.stopWaiting(appFrame);
			}
		};
	}

	private ActionListener getActionListenerForActionRefresh() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bindData();
			}
		};
	}
	
	public Report getParticipationsReport() {
		Report participationsReport = null;
		RichParticipationsTableModel tableModel = table_participations.getParticipationsTableModel();
		if (tableModel != null) {
			RichParticipation[] participations = new RichParticipation[tableModel.getRowCount()];
			Discipline discipline = null;
			for (int row = 0; row < tableModel.getRowCount(); row++) {
				RichParticipation richParticipation = tableModel.getParticipationAtRow(row);
				participations[row] = richParticipation;
				if (richParticipation.getDiscipline() != null) {
					discipline = richParticipation.getDiscipline();
				}
			}
			String title = competition != null ? competition.getTitle() : "";
			participationsReport = ReportFactory.createParticipationsReport(new RichParticipationReportable(participations, title),
					title);
		}
		return participationsReport;
	}

	public Report getKidsStartGridReport() {
		Report kidsReport = null;
		RichParticipationsTableModel tableModel = table_participations.getParticipationsTableModel();
		if (tableModel != null) {
			RichParticipation[] participations = new RichParticipation[tableModel.getRowCount()];
			Discipline discipline = null;
			for (int row = 0; row < tableModel.getRowCount(); row++) {
				RichParticipation richParticipation = tableModel.getParticipationAtRow(row);
				participations[row] = richParticipation;
				if (richParticipation.getDiscipline() != null) {
					discipline = richParticipation.getDiscipline();
				}
			}
			//Sort participations by rank
			Comparator<RichParticipation> COMP = new Comparator<RichParticipation>() {
				@Override
				public int compare(RichParticipation o1, RichParticipation o2) {
					return Integer.compare(o1.getRank(), o2.getRank());
				}
			};
			Arrays.parallelSort(participations, COMP);
			kidsReport = ReportFactory.createKidsStartGridReport(new RichParticipationReportable(participations),
					discipline);
		}
		return kidsReport;
	}
	
	private ActionListener getActionListenerForActionReportKidsStartGrid() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ReportingController.report(getKidsStartGridReport(), appFrame);
			}
		};
	}
	
	private ActionListener getActionListenerForActionCreateParticipationsReport() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ReportingController.report(getParticipationsReport(), appFrame);
			}
		};
	}

	private ActionListener getActionListenerForActionReportDonationsReport() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				actionReportDonationsReport();
			}
		};
	}

	private void actionReportDonationsReport() {
		UIHelper.startWaiting(appFrame);
		try {
			RichParticipationsTableModel tableModel = table_participations.getParticipationsTableModel();
			if (tableModel != null && tableModel.getRowCount() > 0) {
				RichParticipation[] richParticipations = new RichParticipation[tableModel.getRowCount()];
				for (int row = 0; row < tableModel.getRowCount(); row++) {
					RichParticipation richParticipation = tableModel.getParticipationAtRow(row);
					richParticipations[row] = richParticipation;
				}
				RichParticipationReportable reportable = new RichParticipationReportable(
						controller.getRichParticipationsWithDonations(richParticipations),
						appFrame.getTitle());
				ReportingController.report(ReportFactory.createDonationReport(reportable), appFrame);
			}
		} catch (Exception e) {
			appFrame.showUserMessage(e, null);
		} finally {
			UIHelper.stopWaiting(appFrame);
		}
	}

	public Report getDisciplineStartGridReport() {
		Report disciplineStartGridReport = null;
		RichParticipationsTableModel tableModel = table_participations.getParticipationsTableModel();
		if (tableModel != null) {
			RichParticipation[] participations = new RichParticipation[tableModel.getRowCount()];
			Discipline discipline = null;
			for (int row = 0; row < tableModel.getRowCount(); row++) {
				RichParticipation richParticipation = tableModel.getParticipationAtRow(row);
				participations[row] = richParticipation;
				if (richParticipation.getDiscipline() != null) {
					discipline = richParticipation.getDiscipline();
				}
			}
			disciplineStartGridReport = ReportFactory
					.createDisciplineStartGridReport(new RichParticipationReportable(participations), discipline);
		}
		return disciplineStartGridReport;
	}

	private ActionListener getActionListenerForActionReportDisciplineStartGrid() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ReportingController.report(getDisciplineStartGridReport(), appFrame);
			}
		};
	}

	private ActionListener getActionListenerForActionReportParticipationConfirmation() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				RichParticipationsTableModel allParticipations = table_participations.getParticipationsTableModel();
				if (allParticipations != null) {
					RichParticipation[] selectedParticipations =
							allParticipations.getParticipationsAtRows(
									table_participations.getSelectedRows(), table_participations);
					if (selectedParticipations != null) {
						Discipline discipline = null;
						Report[] reportsToGenerate = new Report[selectedParticipations.length];
						int repPos = 0;
						for (RichParticipation currentParticipation : selectedParticipations) {
							if (currentParticipation.getDiscipline() != null) {
								discipline = currentParticipation.getDiscipline();
							}
							Report reportForCurrentParticipation = ReportFactory.createParticipationConfirmationReport(
									new RichParticipationReportable(new RichParticipation[] { currentParticipation }),
									currentParticipation.getParticipant(), discipline);
							reportsToGenerate[repPos] = reportForCurrentParticipation;
							repPos++;
						}
						ReportingController.report(reportsToGenerate, appFrame);
					}
				}
			}
		};
	}

	private ActionListener getActionListenerForActionReportCertification() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				RichParticipationsTableModel tableModel = table_participations.getParticipationsTableModel();
				if (tableModel != null) {
					RichParticipation[] participations = tableModel
							.getParticipationsAtRows(table_participations.getSelectedRows(), table_participations);
					if (participations != null) {
						Discipline discipline = null;
						Report[] reports = new Report[participations.length];
						int repPos = 0;
						for (RichParticipation richP : participations) {
							if (richP.getDiscipline() != null) {
								discipline = richP.getDiscipline();
							}
							Report report = ReportFactory.createCertificationReportMfAc(
									new RichParticipationReportable(new RichParticipation[] { richP }),
									richP.getParticipant(), discipline);
							reports[repPos] = report;
							repPos++;
						}
						ReportingController.report(reports, appFrame);
					}
				}
			}
		};
	}

	private ActionListener getActionListenerForActionReportCertificationMf() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				RichParticipationsTableModel tableModel = table_participations.getParticipationsTableModel();
				if (tableModel != null) {
					RichParticipation[] participations = tableModel
							.getParticipationsAtRows(table_participations.getSelectedRows(), table_participations);
					if (participations != null) {
						Discipline discipline = null;
						Report[] reports = new Report[participations.length];
						int repPos = 0;
						for (RichParticipation richP : participations) {
							if (richP.getDiscipline() != null) {
								discipline = richP.getDiscipline();
							}
							Report report = ReportFactory.createCertificationMfReport(
									new RichParticipationReportable(new RichParticipation[] { richP }),
									richP.getParticipant(), discipline);
							reports[repPos] = report;
							repPos++;
						}
						ReportingController.report(reports, appFrame);
					}
				}
			}
		};
	}

	private ActionListener getActionListenerForActionReportCertificationKids() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				RichParticipationsTableModel tableModel = table_participations.getParticipationsTableModel();
				if (tableModel != null) {
					RichParticipation[] participations = tableModel
							.getParticipationsAtRows(table_participations.getSelectedRows(), table_participations);
					if (participations != null) {
						Discipline discipline = null;
						Report[] reports = new Report[participations.length];
						int repPos = 0;
						for (RichParticipation richP : participations) {
							if (richP.getDiscipline() != null) {
								discipline = richP.getDiscipline();
							}
							Report report = ReportFactory.createCertificationKidsReport(
									new RichParticipationReportable(new RichParticipation[] { richP }),
									richP.getParticipant(), discipline);
							reports[repPos] = report;
							repPos++;
						}
						ReportingController.report(reports, appFrame);
					}
				}
			}
		};
	}

	public Report[] getDisciplineGenderStartGridReports() {
		Report[] disciplineGenderStartGridReports = null;
		RichParticipationsTableModel tableModel = table_participations.getParticipationsTableModel();
		if (tableModel != null) {
			Discipline discipline = null;
			RichParticipation[] participations = new RichParticipation[tableModel.getRowCount()];
			for (int row = 0; row < tableModel.getRowCount(); row++) {
				participations[row] = tableModel.getParticipationAtRow(row);
				if (participations[row].getDiscipline() != null) {
					discipline = participations[row].getDiscipline();
				}
			}
			HashMap<String, Object> parameters_female = new HashMap<String, Object>();
			parameters_female.put(KeywordsDictionary.CONTACT_GENDER, Contact.gendertype.FEMALE.toString());
			Report report_female = ReportFactory.createDisciplineGenderStartGridReport(
					new RichParticipationReportable(participations), parameters_female, discipline);
			HashMap<String, Object> parameters_male = new HashMap<String, Object>();
			parameters_male.put(KeywordsDictionary.CONTACT_GENDER, Contact.gendertype.MALE.toString());
			Report report_male = ReportFactory.createDisciplineGenderStartGridReport(
					new RichParticipationReportable(participations), parameters_male, discipline);
			disciplineGenderStartGridReports = new Report[] { report_female, report_male };
		}
		return disciplineGenderStartGridReports;
	}

	private ActionListener getActionListenerForActionReportDisciplineGenderStartGrid() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ReportingController.report(getDisciplineGenderStartGridReports(), appFrame);
			}
		};
	}

	public Report[] getDisciplineAgeClassStartGridReports() {
		Report[] disciplineAgeClassStartGridReports = null;
		RichParticipationsTableModel tableModel = table_participations.getParticipationsTableModel();
		if (tableModel != null && tableModel instanceof RichParticipationsTableModel) {
			RichParticipation[] participations = tableModel.getAllParticipations();
			AgeClass[] ageClasses = controller.getDistinctAgeClasses(participations);
			Report[] reports = null;
			if (ageClasses != null && ageClasses.length > 0) {
				reports = new Report[ageClasses.length];
				int acCounter = 0;
				for (AgeClass ageClass : ageClasses) {
					if (ageClass != null) {
						Discipline discipline = null;
						HashMap<String, Object> parameters = new HashMap<String, Object>();
						String ageClassName = ageClass.toString();
						parameters.put(KeywordsDictionary.AGECLASS, ageClassName);
						RichParticipation[] acParticipations = controller.getParticipationsForAgeClass(participations,
								ageClass);
						if (acParticipations != null && acParticipations.length > 0) {
							discipline = acParticipations[0].getDiscipline();
						}
						Report acReport = ReportFactory.createDisciplineAgeClassStartGridReport(
								new RichParticipationReportable(acParticipations), parameters, discipline);
						reports[acCounter] = acReport;
						acCounter++;
					}
				}
			}
			disciplineAgeClassStartGridReports = reports;
		}
		return disciplineAgeClassStartGridReports;
	}

	private ActionListener getActionListenerForActionReportDisciplineAgeClassStartGrid() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ReportingController.report(getDisciplineAgeClassStartGridReports(), appFrame);
			}
		};
	}

	public Report[] getDisciplineGenderAgeClassStartGridReports() {
		Report[] disciplineGenderAgeClassStartGridReports = null;
		RichParticipationsTableModel tableModel = table_participations.getParticipationsTableModel();
		if (tableModel != null && tableModel instanceof RichParticipationsTableModel) {
			ArrayList<Report> reportsList = new ArrayList<Report>(0);
			RichParticipation[] participations = tableModel.getAllParticipations();
			AgeClass[] ageClasses = controller.getDistinctAgeClasses(participations);
			if (ageClasses != null && ageClasses.length > 0) {
				for (AgeClass ageClass : ageClasses) {
					if (ageClass != null) {
						Discipline discipline = null;
						RichParticipation[] acParticipations = controller.getParticipationsForAgeClass(participations,
								ageClass);
						String ageClassName = ageClass.toString();
						RichParticipation[] acFemaleParticipations = controller
								.getParticipationsForGender(acParticipations, Contact.gendertype.FEMALE);
						if (acParticipations != null && acParticipations.length > 0) {
							discipline = acParticipations[0].getDiscipline();
						}
						if (acFemaleParticipations != null && acFemaleParticipations.length > 0) {
							HashMap<String, Object> parameters_female = new HashMap<String, Object>();
							parameters_female.put(KeywordsDictionary.AGECLASS, ageClassName);
							parameters_female.put(KeywordsDictionary.CONTACT_GENDER_RESOURCE,
									Messages.getString(Messages.FEMALE));
							Report acFemaleReport = ReportFactory.createDisciplineGenderAgeClassStartGridReport(
									new RichParticipationReportable(acFemaleParticipations), parameters_female,
									discipline);
							reportsList.add(acFemaleReport);
						}
						RichParticipation[] acMaleParticipations = controller
								.getParticipationsForGender(acParticipations, Contact.gendertype.MALE);
						if (acFemaleParticipations != null && acMaleParticipations.length > 0) {
							HashMap<String, Object> parameters_male = new HashMap<String, Object>();
							parameters_male.put(KeywordsDictionary.AGECLASS, ageClassName);
							parameters_male.put(KeywordsDictionary.CONTACT_GENDER_RESOURCE,
									Messages.getString(Messages.MALE));
							Report acMaleReport = ReportFactory.createDisciplineGenderAgeClassStartGridReport(
									new RichParticipationReportable(acMaleParticipations), parameters_male, discipline);
							reportsList.add(acMaleReport);
						}
					}
				}
			}
			Report[] reports = new Report[0];
			reports = reportsList.toArray(reports);
			disciplineGenderAgeClassStartGridReports = reports;
		}
		return disciplineGenderAgeClassStartGridReports;
	}

	public Report getTeamRankingReport() {
		Report teamRankingReport = null;
		RichParticipationsTableModel tableModel = table_participations.getParticipationsTableModel();
		if (tableModel != null) {
			Discipline discipline = null;
			RichParticipation[] participations = tableModel.getAllParticipations();
			if (participations != null && participations.length > 0) {
				discipline = participations[0].getDiscipline();
			}
			teamRankingReport = ReportFactory.createTeamRankingReport(new TeamRankingReportable(participations),
					discipline);
		}
		return teamRankingReport;
	}

	private ActionListener getActionListenerForActionReportTeamRanking() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ReportingController.report(getTeamRankingReport(), appFrame);
			}
		};
	}

	private ActionListener getActionListenerForActionReportDisciplineGenderAgeClassStartGrid() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ReportingController.report(getDisciplineGenderAgeClassStartGridReports(), appFrame);
			}
		};
	}

	private ActionListener getActionListenerForActionCreateAllDisciplineStartGridReports() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Report[] reports = new Report[0];
				reports = (Report[]) ArrayUtils.add(reports, getDisciplineStartGridReport());
				reports = (Report[]) ArrayUtils.addAll(reports, getDisciplineGenderStartGridReports());
				reports = (Report[]) ArrayUtils.addAll(reports, getDisciplineAgeClassStartGridReports());
				reports = (Report[]) ArrayUtils.addAll(reports, getDisciplineGenderAgeClassStartGridReports());
				reports = (Report[]) ArrayUtils.addAll(reports, new Report[] { getTeamRankingReport() });
				ReportingController.report(reports, appFrame);
			}
		};
	}

	private ActionListener getActionListenerForActionAddParticipant() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ParticipationFormDialog modelEditor = new ParticipationFormDialog(competition, getSelectedDiscipline(),
						null, appFrame, controller);
				modelEditor.setVisible(true);
				if (modelEditor.isOperationSucceeded()) {
					bindData();
				}
			}
		};
	}

	private ActionListener getActionListenerForActionEditParticipation() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				editSelectedParticipation();
			}
		};
	}

	private void editSelectedParticipation() {
		Participation participation = table_participations.getSelectedParticipation();
		if (participation != null) {
			Discipline disciplineForEditDialog = getSelectedDiscipline();
			Discipline[] disciplines = getSelectedDisciplines();
			if (disciplines != null && disciplines.length > 1) {
				for (int i = 0; i < disciplines.length; i++) {
					if (disciplines[i].getId() == participation.getDisciplineId()) {
						disciplineForEditDialog = disciplines[i];
						break;
					}
				}
			}
			ParticipationFormDialog modelEditor = new ParticipationFormDialog(competition, disciplineForEditDialog,
					participation, appFrame, controller);
			modelEditor.setVisible(true);
			if (modelEditor.isOperationSucceeded()) {
				bindData();
			}
		} else {
			appFrame.showUserMessage(JOptionPane.WARNING_MESSAGE,
					Messages.getString(Messages.NO_PARTICIPATION_SELECTED_SELECT_ONE_FIRST), competition);
		}
	}

	private ActionListener getActionListenerForActionRemoveParticipation() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				UIHelper.startWaiting(appFrame);
				Participation participation = table_participations.getSelectedParticipation();
				if (participation != null) {
					try {
						if (UIHelper.showSureRequest(
								Messages.getString(Messages.REMOVE) + " " + Messages.getString(Messages.PARTICIPATION),
								appFrame)) {
							controller.deleteModel(participation);
							bindData();
						}
					} catch (Exception e1) {
						appFrame.showUserMessage(e1, participation);
					}
				} else {
					appFrame.showUserMessage(JOptionPane.WARNING_MESSAGE,
							Messages.getString(Messages.NO_PARTICIPATION_SELECTED_SELECT_ONE_FIRST), competition);
				}
				UIHelper.stopWaiting(appFrame);
			}
		};
	}

	private ActionListener getActionListenerForActionRemoveAllParticipantsFromDiscipline() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				UIHelper.startWaiting(appFrame);
				if (UIHelper.showSureRequest(Messages.getString(Messages.REMOVE_ALL_PARTICIPATIONS_FROM_DISCIPLINE),
						appFrame)) {
					RichParticipationsTableModel participationsTM = (RichParticipationsTableModel) table_participations
							.getModel();
					if (participationsTM != null && participationsTM.getAllParticipations() != null) {
						try {
							Participation[] participations = participationsTM.getAllParticipations();
							for (Participation participation : participations) {
								controller.deleteModel(participation);
							}
							bindData();
						} catch (Exception e1) {
							appFrame.showUserMessage(e1, getSelectedDiscipline());
						}
					} else {
						appFrame.showUserMessage(JOptionPane.WARNING_MESSAGE,
								Messages.getString(Messages.NO_DISCIPLINE_SELECTED_SELECT_ONE), competition);
					}
				}
				UIHelper.stopWaiting(appFrame);
			}
		};
	}

	private ActionListener getActionListenerForActionRemoveAllParticipantsFromCompetition() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				UIHelper.startWaiting(appFrame);
				if (UIHelper.showSureRequest(Messages.getString(Messages.REMOVE_ALL_PARTICIPATIONS_FROM_COMPETITION),
						appFrame)) {
					try {
						ParticipationListModel participationListModel = controller
								.getAllParticipationsForCompetition(competition);
						if (participationListModel != null && participationListModel.getModel() != null) {
							Participation[] participations = participationListModel.getModel();
							for (Participation participation : participations) {
								controller.deleteModel(participation);
							}
							bindData();
						}
					} catch (Exception e1) {
						appFrame.showUserMessage(e1, getSelectedDiscipline());
					}
				}
				UIHelper.stopWaiting(appFrame);
			}
		};
	}

	public Discipline getSelectedDiscipline() {
		Object selectedObject = list_disciplines.getSelectedValue();
		if (selectedObject != null && selectedObject instanceof Discipline) {
			return (Discipline) selectedObject;
		} else {
			return null;
		}
	}

	public Discipline[] getSelectedDisciplines() {
		List<Discipline> selectedObjects = list_disciplines.getSelectedValuesList();
		if (selectedObjects != null && selectedObjects.size() > 0) {
			Discipline[] disciplines = new Discipline[selectedObjects.size()];
			for (int sNo = 0; sNo < selectedObjects.size(); sNo++) {
				disciplines[sNo] = selectedObjects.get(sNo);
			}
			return disciplines;
		} else {
			return null;
		}
	}

	public void setSelectedDisciplines(Discipline[] disciplines) {
		int[] modelIndicesToSelect = null;
		if (disciplines != null && disciplines.length > 0 && list_disciplines != null
				&& list_disciplines.getModel() != null && list_disciplines.getModel() instanceof DisciplineListModel) {
			modelIndicesToSelect = new int[disciplines.length];
			for (int pos = 0; pos < disciplines.length; pos++) {
				Discipline discipline = disciplines[pos];
				Model disciplineInList = ((DisciplineListModel) list_disciplines.getModel())
						.getItemByModelId(discipline.getId());
				list_disciplines.setSelectedValue(disciplineInList, disciplines.length == 1);
				modelIndicesToSelect[pos] = list_disciplines.getSelectedIndex();
			}
			list_disciplines.setSelectedIndices(modelIndicesToSelect);
		}
	}

	public void setSelectedDiscipline(Discipline discipline) {
		setSelectedDisciplines(new Discipline[] { discipline });
	}

	public void optimizeSplit() {
		splitPane.setDividerLocation(list_disciplines.getPreferredSize().width + 5);
	}

	public KeyListener getKeyListenerForActionSearchModel() {
		return new KeyListener() {

			@Override
			public void keyTyped(KeyEvent arg0) {
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
			}

			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					searchModel();
				} else if (arg0.getKeyCode() == KeyEvent.VK_DOWN) {
					selectFirstModelFromModelList();
				}
			}
		};
	}

	public KeyListener getKeyListenerForParticipationsActions() {
		return new KeyListener() {

			@Override
			public void keyTyped(KeyEvent arg0) {
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
			}

			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_F7) {
					editModel();
				} else if (arg0.getKeyCode() == KeyEvent.VK_ALT + KeyEvent.VK_F3) {
					focusSearchField();
				} else if (arg0.getKeyCode() == KeyEvent.VK_F9) {
					focusDisciplinesList();
				} else if (arg0.getKeyCode() == KeyEvent.VK_F10) {
					focusParticipations();
				}
			}
		};
	}

	public ActionListener getActionListenerForActionSearchModel() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				searchModel();
			}
		};
	}

	public void selectFirstModelFromModelList() {
		list_disciplines.grabFocus();
	}

	public ActionListener getActionListenerForActionResetDisciplineGrid() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				UIHelper.startWaiting(appFrame);
				resetDisciplineGrid(true, true);
				UIHelper.stopWaiting(appFrame);
			}
		};
	}

	public ActionListener getActionListenerForActionResetDisciplineRankings() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				UIHelper.startWaiting(appFrame);
				resetDisciplineGrid(true, false);
				UIHelper.stopWaiting(appFrame);
			}
		};
	}

	public ActionListener getActionListenerForActionImportParticipationData() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				UIHelper.startWaiting(appFrame);
				try {
					if (UIHelper.showSureRequest(Messages.getString(Messages.IMPORT_PARTICIPATION_DATA), appFrame)) {
						importParticipationData();
					}
				} catch (Exception e) {
					appFrame.showUserMessage(e, null);
				}
				UIHelper.stopWaiting(appFrame);
			}
		};
	}

	public void importParticipationData() throws Exception {
		Participation[] importedParticipations = controller.importParticipationsData(null);
		if (importedParticipations != null) {
			bindData();
			appFrame.showUserMessage(JOptionPane.INFORMATION_MESSAGE, "Eingelesen: " + importedParticipations.length,
					importedParticipations[0]);
		} else {
			appFrame.showUserMessage(JOptionPane.INFORMATION_MESSAGE, "Nix zum importieren", null);
		}
	}

	public ActionListener getActionListenerForActionExportParticipationData() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				UIHelper.startWaiting(appFrame);
				try {
					exportParticipationData();
				} catch (Exception e) {
					appFrame.showUserMessage(e, null);
				}
				UIHelper.stopWaiting(appFrame);
			}
		};
	}

	public void exportParticipationData() throws Exception {
		Participation[] exportParticipations = table_participations.getParticipationsTableModel()
				.getAllParticipations();
		if (exportParticipations != null) {
			boolean success = controller.exportParticipationsData(exportParticipations, null);
			appFrame.showUserMessage(JOptionPane.INFORMATION_MESSAGE, "Exportiert: " + success,
					exportParticipations[0]);
		} else {
			appFrame.showUserMessage(JOptionPane.INFORMATION_MESSAGE, "Nix zum exportieren", null);
		}
	}

	public void resetDisciplineGrid(boolean resetRankings, boolean resetResultTimes) {
		if (UIHelper.showSureRequest(Messages.getString(Messages.RESET_DISCIPLINE_GRID), appFrame)) {
			TableModel tableModel = table_participations.getModel();
			if (tableModel != null && tableModel instanceof RichParticipationsTableModel) {
				RichParticipationsTableModel richParticipationsTableModel = (RichParticipationsTableModel) tableModel;
				RichParticipation[] richParticipations = richParticipationsTableModel.getAllParticipations();
				for (RichParticipation richParticipation : richParticipations) {
					if (resetRankings) {
						richParticipation.setAgeClassRank(0);
						richParticipation.setGenderAgeClassRank(0);
						richParticipation.setGenderRank(0);
						richParticipation.setRank(0);
					}
					if (resetResultTimes) {
						richParticipation.setResultTimeAsString(null);
					}
					try {
						controller.saveModel(richParticipation);
					} catch (Exception e) {
						appFrame.showUserMessage(e, richParticipation);
					}
				}
			}
		}
	}

	public void searchModel() {
		String searchValue = textfield_search.getText();
		TableModel tableModel = table_participations.getModel();
		if (tableModel != null && tableModel instanceof RichParticipationsTableModel) {
			RichParticipationsTableModel richParticipationsTableModel = (RichParticipationsTableModel) tableModel;
			// case 1: no search value given
			if (searchValue == null || searchValue.length() == 0) {
				table_participations.selectAll();
			} else {
				// case 2: search value given, execute search
				table_participations.getSelectionModel().clearSelection();
				RichParticipation[] richParticipations = richParticipationsTableModel.getAllParticipations();
				for (RichParticipation richParticipation : richParticipations) {
					if (Utils.isSearchStringInsideOfRichParticipation(searchValue, richParticipation)) {
						int rowOfParticipation = richParticipationsTableModel.getRowOfParticipation(richParticipation);
						if (rowOfParticipation >= 0) {
							table_participations.getSelectionModel().addSelectionInterval(rowOfParticipation,
									rowOfParticipation);
						}
					}
				}
			}
		}
	}

	public void editModel() {
		if (table_participations.getSelectedRowCount() == 1) {
			editSelectedParticipation();
		}
	}

	public void focusSearchField() {
		textfield_search.requestFocus();
	}

	public void focusDisciplinesList() {
		list_disciplines.grabFocus();
	}

	public void focusParticipations() {
		table_participations.grabFocus();
	}

	public Color getTabColorForStatus() {
		if (competition == null) {
			return Color.RED;
		}
		Calendar today = Calendar.getInstance();
		Calendar competitionStart = Calendar.getInstance();
		competitionStart.setTime(competition.getStartOfCompetition());
		if (DateUtils.isSameDay(new Date(), competition.getStartOfCompetition())) {
			return Color.GREEN;
		}
		if (today.after(competitionStart)) {
			return Color.GRAY;
		}
		if (today.before(competitionStart)) {
			return Color.YELLOW;
		}
		return Color.RED;
	}

}
