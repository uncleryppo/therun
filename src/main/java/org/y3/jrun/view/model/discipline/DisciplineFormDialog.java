package org.y3.jrun.view.model.discipline;

import org.y3.jrun.control.ApplicationController;
import org.y3.jrun.model.discipline.Discipline;
import org.y3.jrun.view.ApplicationFrame;
import org.y3.jrun.view.gfx.IconDictionary;
import org.y3.jrun.view.model.ModelForm;
import org.y3.jrun.view.model.ModelFormDialog;

/**
 * Copyright: 2011 - 2020
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class DisciplineFormDialog extends ModelFormDialog {

    private static final long serialVersionUID = 1L;

    public DisciplineFormDialog(Discipline _model,
            ApplicationFrame applicationFrame,
            ApplicationController applicationController) {
        super(_model, applicationFrame, applicationController);
        setIconImage(IconDictionary.getImageIcon(IconDictionary.DISCIPLINE).getImage());
    }

    @Override
    public Integer getFormWidthHint() {
        return 400;
    }

    @Override
    public ModelForm getModelForm() {
        modelForm = new DisciplineForm(ModelForm.MODE.EDITOR);
        return modelForm;
    }

    @Override
    public boolean actionSave() {
        Discipline discipline = (Discipline) modelForm.getModel();
        try {
            return controller.saveModel(discipline);
        } catch (Exception e) {
            appFrame.showUserMessage(e, discipline);
        }
        return false;
    }

    @Override
    public Discipline getModel() {
        if (modelForm != null) {
            return (Discipline) modelForm.getModel();
        } else {
            return null;
        }
    }

}
