/**
 * 
 */
package org.y3.jrun.view.model.team;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.y3.jrun.control.ApplicationController;
import org.y3.jrun.model.Model;
import org.y3.jrun.model.team.Team;
import org.y3.jrun.model.team.TeamListModel;
import org.y3.jrun.view.ApplicationFrame;
import org.y3.jrun.view.gfx.IconDictionary;
import org.y3.jrun.view.gfx.UIHelper;
import org.y3.jrun.view.i18n.Messages;
import org.y3.jrun.view.model.ModelForm;
import org.y3.jrun.view.model.ModelPanel;

/**
 * @author Ryppo
 *
 */
public class TeamPanel extends ModelPanel {
    
    private static final long serialVersionUID = 1L;
    private TeamComparator teamComparator;
    
    public TeamPanel(ApplicationController _controller,
	    ApplicationFrame _appFrame) {
	super(_controller, _appFrame);
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.view.model.ModelPanel#getPanelTitle()
     */
    @Override
    public String getPanelTitle() {
	return Messages.getString(Messages.TEAM);
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.view.model.ModelPanel#createModelForm()
     */
    @Override
    protected void createModelForm() {
	modelForm = new TeamForm(ModelForm.MODE.VIEWER);
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.view.model.ModelPanel#addFunctionsToMenuAndButtonPanel()
     */
    @Override
    protected void addFunctionsToMenuAndButtonPanel() {
	addFunctionToMenuAndButtonPanelForActionNewModel();
	addFunctionToMenuAndButtonPanelForActionRemoveModel();
	addFunctionToMenuAndButtonPanelForActionSaveModel();
	addSeparatorToMenuAndButtonPanel(true);
	addFunctionToMenuAndButtonPanel(IconDictionary.getImageIcon(IconDictionary.PARTICIPATIONS), 
		Messages.getString(Messages.SHOW_PARTICIPATIONS), 
		getActionListenerForActionShowParticipationForSelectedTeam(), false);
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.view.model.ModelPanel#bindData()
     */
    @Override
    public void bindData() throws IOException, ClassNotFoundException,
	    SQLException {
	UIHelper.startWaiting(appFrame);
	TeamListModel model = controller.getAllTeams();
	Team team = (Team) modelForm.getModel();
	jp_modelList.setModel(model);
	if (model == null || model.getSize() == 0) {
	    modelForm.setModel(null);
	} else {
	    jp_modelList.setSelectedValue(team, true);
	}
	UIHelper.stopWaiting(appFrame);
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.view.model.ModelPanel#getSpecificStatisticsPanel()
     */
    @Override
    public JPanel getSpecificStatisticsPanel() {
	// TODO Auto-generated method stub
	return null;
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.view.model.ModelPanel#bindDataForModelById(int)
     */
    @Override
    public void bindDataForModelById(int teamId) throws IOException,
	    ClassNotFoundException, SQLException {
	UIHelper.startWaiting(appFrame);
	TeamListModel model = controller.getTeamById(teamId);
	jp_modelList.setModel(model);
	if (model == null || model.getSize() == 0) {
	    modelForm.setModel(null);
	} else {
	    jp_modelList.setSelectedValue(model.getElementAt(0), true);
	}
	UIHelper.stopWaiting(appFrame);
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.view.model.ModelPanel#actionSortModels()
     */
    @Override
    protected void actionSortModels() {
	UIHelper.startWaiting(appFrame);
	if (teamComparator == null) {
	    teamComparator = new TeamComparator();
	}
	((TeamListModel) jp_modelList.getModel()).sort(teamComparator);
	jp_modelList.repaint();
	UIHelper.stopWaiting(appFrame);
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.view.model.ModelPanel#getIcon()
     */
    @Override
    public Icon getIcon() {
	return IconDictionary.getImageIcon(IconDictionary.TEAM);
    }
    
    public void actionShowParticipationForSelectedTeam() {
	Model object = jp_modelList.getSelectedModel();
	if (object != null && object instanceof Team) {
		appFrame.showParticipationsByTeamId(object.getId());
	} else {
		appFrame.showUserMessage(JOptionPane.WARNING_MESSAGE, 
				Messages.getString(Messages.NO_TEAM_SELECTED_SELECT_ONE), null);
	}
    }
    
    public ActionListener getActionListenerForActionShowParticipationForSelectedTeam() {
	return new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			actionShowParticipationForSelectedTeam();
		}
	};
}

    @Override
    public void resetFilters() {
    }

}
