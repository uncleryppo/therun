package org.y3.jrun.view.model.competition;

import java.util.Comparator;
import java.util.Date;

import org.y3.jrun.model.competition.Competition;

public class CompetitionComparator implements Comparator<Competition> {

    enum COMPARE_BY {NAME_ASC, DATE_DESC}

    private COMPARE_BY compareMode = COMPARE_BY.NAME_ASC;

    public CompetitionComparator(COMPARE_BY _compareMode) {
        compareMode = _compareMode;
    }

    private int compareByNameAsc(Competition competition1, Competition competition2) {
        String c1 = competition1.toString();
        String c2 = competition2.toString();
        return c1.compareTo(c2);
    }
    private int compareByDateDesc(Competition competition1, Competition competition2) {
    	Date d1 = competition1.getStartOfCompetition();
    	Date d2 = competition2.getStartOfCompetition();
        int asc = d1 == null ? 1 : d1.compareTo(d2);
        return asc == 0 ? 0 : asc * (-1);
    }

    @Override
    public int compare(Competition competition1, Competition competition2) {
		if (competition1 == null && competition2 == null) {
			return 0;
		} else if (competition1 == null) {
			return 1;
		} else if (competition2 == null) {
			return -1;
		} else {
			switch (compareMode) {
				case DATE_DESC:
					return compareByDateDesc(competition1, competition2);
				case NAME_ASC:
				default:
					return compareByNameAsc(competition1, competition2);
			}
		}
    }

}
