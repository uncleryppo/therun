package org.y3.jrun.view.model.participation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import javax.swing.ComboBoxModel;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.y3.jrun.control.ApplicationController;
import org.y3.jrun.model.Model;
import org.y3.jrun.model.ModelListModel;
import org.y3.jrun.model.competition.Competition;
import org.y3.jrun.model.contact.Contact;
import org.y3.jrun.model.contact.ContactListModel;
import org.y3.jrun.model.discipline.Discipline;
import org.y3.jrun.model.participation.Participation;
import org.y3.jrun.model.participation.ParticipationListModel;
import org.y3.jrun.model.participation.RichParticipation;
import org.y3.jrun.model.participation.RichParticipationReportable;
import org.y3.jrun.model.report.Report;
import org.y3.jrun.model.team.Team;
import org.y3.jrun.view.ApplicationFrame;
import org.y3.jrun.view.gfx.IconDictionary;
import org.y3.jrun.view.gfx.UIHelper;
import org.y3.jrun.view.i18n.Messages;
import org.y3.jrun.view.model.ModelForm;
import org.y3.jrun.view.model.ModelPanel;
import org.y3.jrun.view.model.ModelView_helper;
import org.y3.jrun.view.reporting.ReportFactory;
import org.y3.jrun.view.reporting.ReportingController;

public class ParticipationsPanel extends ModelPanel {

	private static final long serialVersionUID = 1L;
	
	private JComboBox jcb_competition, jcb_contact, jcb_discipline, jcb_team;
        private JCheckBox jcx_includeNonCompetitive, jcx_includeCanceled, jcx_includeNotStarted;
	ParticipationListModel model;

	public ParticipationsPanel(ApplicationController _controller,
			ApplicationFrame _appFrame) {
		super(_controller, _appFrame);
	}

	@Override
	protected void createModelForm() {
		modelForm = new ParticipationForm(controller, ModelForm.MODE.VIEWER, appFrame);
	}

	@Override
	public void bindData() throws IOException, ClassNotFoundException,
			SQLException {
		UIHelper.startWaiting(appFrame);
		Participation participation = (Participation) modelForm.getModel();
		jcb_competition.setModel((ComboBoxModel) controller
				.getAllCompetitions());
		jcb_contact.setModel((ComboBoxModel) controller.getAllContacts());
		jcb_discipline.setModel((ComboBoxModel) controller.getAllDisciplines());
		jcb_team.setModel((ComboBoxModel) controller.getAllTeams());
                jcx_includeNonCompetitive.setSelected(true);
                jcx_includeCanceled.setSelected(true);
                jcx_includeNotStarted.setSelected(true);
		model = controller.getAllParticipations();
		fillParticipationsByMetaData();
		jp_modelList.setModel(model);
                modelForm.bindData();
		if (model == null || model.getSize() == 0) {
			modelForm.setModel(null);
		} else {
			jp_modelList.setSelectedValue(participation, true);
		}
		UIHelper.stopWaiting(appFrame);
	}

	@Override
	public void bindDataForModelById(int modelId) throws IOException,
			ClassNotFoundException, SQLException {
		UIHelper.startWaiting(appFrame);
		ParticipationListModel model = controller.getParticipationById(modelId);
		jp_modelList.setModel(model);
		if (model == null || model.getSize() == 0) {
			modelForm.setModel(null);
		} else {
			jp_modelList.setSelectedValue(model.getElementAt(0), true);
		}
		UIHelper.stopWaiting(appFrame);
	}
        
        @Override
        public void resetFilters() {
            jcb_competition.setSelectedIndex(-1);
            jcb_contact.setSelectedIndex(-1);
            jcb_discipline.setSelectedIndex(-1);
            jcb_team.setSelectedIndex(-1);
            jcx_includeNonCompetitive.setSelected(true);
            jcx_includeCanceled.setSelected(true);
            jcx_includeNotStarted.setSelected(true);
        }
	
	public void bindDataByCompetitionId(int competitionId) throws IOException,
	ClassNotFoundException, SQLException {
		UIHelper.startWaiting(appFrame);
                resetFilters();
		ComboBoxModel cbmodel = jcb_competition.getModel();
		if (cbmodel != null && cbmodel instanceof ModelListModel) {
			((ModelListModel) cbmodel).setSelectedModelByID(competitionId);
			actionFilterModelList();
		}
		UIHelper.stopWaiting(appFrame);
	}
	
	public void bindDataByContactId(int contactId) throws IOException,
	ClassNotFoundException, SQLException {
		UIHelper.startWaiting(appFrame);
                resetFilters();
		ComboBoxModel cbmodel = jcb_contact.getModel();
		if (cbmodel != null && cbmodel instanceof ModelListModel) {
			((ModelListModel) cbmodel).setSelectedModelByID(contactId);
			actionFilterModelList();
		}
		UIHelper.stopWaiting(appFrame);
	}
	
	public void bindDataByDisciplineId(int disciplineId) throws IOException,
	ClassNotFoundException, SQLException {
		UIHelper.startWaiting(appFrame);
                resetFilters();
		ComboBoxModel cbmodel = jcb_discipline.getModel();
		if (cbmodel != null && cbmodel instanceof ModelListModel) {
			((ModelListModel) cbmodel).setSelectedModelByID(disciplineId);
			actionFilterModelList();
		}
		UIHelper.stopWaiting(appFrame);
	}
	
	public void bindDataByTeamId(Integer teamId) throws IOException,
	ClassNotFoundException, SQLException {
		UIHelper.startWaiting(appFrame);
                resetFilters();
		ComboBoxModel cbmodel = jcb_team.getModel();
		if (cbmodel != null && cbmodel instanceof ModelListModel) {
			((ModelListModel) cbmodel).setSelectedModelByID(teamId);
			actionFilterModelList();
		}
		UIHelper.stopWaiting(appFrame);
	}

	@Override
	protected void actionSortModels() {
		// TODO Auto-generated method stub

	}
	
	private void actionShowCompetition() {
		UIHelper.startWaiting(appFrame);
		Model model = jp_modelList.getSelectedModel();
		if (model != null && model instanceof Participation) {
			Participation participation = (Participation) model;
			appFrame.showCompetitionById(participation.getCompetitionId());
		} else {
			appFrame.showUserMessage(JOptionPane.WARNING_MESSAGE, Messages.getString(Messages.NO_PARTICIPATION_SELECTED_SELECT_ONE_FIRST), null);
		}
		UIHelper.stopWaiting(appFrame);
	}
	
	private void actionShowTeam() {
	    UIHelper.startWaiting(appFrame);
	    Model model = jp_modelList.getSelectedModel();
	    if (model != null && model instanceof Participation) {
		Participation participation = (Participation) model;
		appFrame.showTeamById(participation.getTeamId());
	    } else {
		appFrame.showUserMessage(JOptionPane.WARNING_MESSAGE, Messages.getString(Messages.NO_PARTICIPATION_SELECTED_SELECT_ONE_FIRST), null);
	    }
	    UIHelper.stopWaiting(appFrame);
	}
	
	private void actionShowCompetitionInstance() {
		UIHelper.startWaiting(appFrame);
		Model model = jp_modelList.getSelectedModel();
		if (model != null && model instanceof Participation) {
			Participation participation = (Participation) model;
			appFrame.showCompetitionInstanceById(participation);
		} else {
			appFrame.showUserMessage(JOptionPane.WARNING_MESSAGE, Messages.getString(Messages.NO_PARTICIPATION_SELECTED_SELECT_ONE_FIRST), null);
		}
		UIHelper.stopWaiting(appFrame);
	}
	
	private void actionShowContact() {
		UIHelper.startWaiting(appFrame);
		Model model = jp_modelList.getSelectedModel();
		if (model != null && model instanceof Participation) {
			Participation participation = (Participation) model;
			appFrame.showContactById(participation.getContactId());
		} else {
			appFrame.showUserMessage(JOptionPane.WARNING_MESSAGE, Messages.getString(Messages.NO_PARTICIPATION_SELECTED_SELECT_ONE_FIRST), null);
		}
		UIHelper.stopWaiting(appFrame);
	}

	public void actionFilterModelList() {
		UIHelper.startWaiting(appFrame);
		try {
			Contact contact = null;
			Competition competition = null;
			Discipline discipline = null;
			Team team = null;
			Object filterCompetition = jcb_competition.getSelectedItem();
			Object filterContact = jcb_contact.getSelectedItem();
			Object filterDiscipline = jcb_discipline.getSelectedItem();
			Object filterTeam = jcb_team.getSelectedItem();
			if (filterCompetition != null
					&& filterCompetition instanceof Competition) {
				competition = (Competition) filterCompetition;
			}
			if (filterContact != null && filterContact instanceof Contact) {
				contact = (Contact) filterContact;
			}
			if (filterDiscipline != null && filterDiscipline instanceof Discipline) {
				discipline = ((Discipline) filterDiscipline);
			}
			if (filterTeam != null && filterTeam instanceof Team) {
			    	team = ((Team) filterTeam);
			}
			model = controller.getAllParticipationsForCompetitionAndContactAndDisciplineAndTeam(
					competition, contact, discipline, team,
                                jcx_includeNonCompetitive.isSelected(),
                                jcx_includeCanceled.isSelected(),
                                jcx_includeNotStarted.isSelected()
                                );
			fillParticipationsByMetaData();
			Participation participation = (Participation) modelForm.getModel();
			jp_modelList.setModel(model);
			if (model == null || model.getSize() == 0) {
				modelForm.setModel(null);
			} else {
				jp_modelList.setSelectedValue(participation, true);
			}
			ModelView_helper.setSelectedModelInComboBox(jcb_competition, competition);
			ModelView_helper.setSelectedModelInComboBox(jcb_contact, contact);
			ModelView_helper.setSelectedModelInComboBox(jcb_discipline, discipline);
			ModelView_helper.setSelectedModelInComboBox(jcb_team, team);
		} catch (Exception e) {
			appFrame.showUserMessage(e, null);
		}
		UIHelper.stopWaiting(appFrame);
	}
	
	@Override
	public void actionNewModel() {
		super.actionNewModel();
		if (modelForm instanceof ParticipationForm) {
			((ParticipationForm) modelForm).setSelectedCompetitonAnContact(
					(Competition) jcb_competition.getSelectedItem(),
					(Contact) jcb_contact.getSelectedItem());
		}
	}
	
	private void fillParticipationsByMetaData() throws IOException, ClassNotFoundException, SQLException {
		UIHelper.startWaiting(appFrame);
		if (model != null && model.getSize() != 0) {
			for (int i = 0; i < model.getSize(); i++) {
				Participation participation = model.getElementAt(i);
				ContactListModel contacts = controller.getContactById(participation.getContactId());
				if (contacts != null && contacts.getSize() != 0) {
					String participantName = contacts.getElementAt(0).toString(); 
					participation.setParticipantName(participantName);
				}
			}
		}
		UIHelper.stopWaiting(appFrame);
	}

	@Override
	public String getPanelTitle() {
		return Messages.getString(Messages.PARTICIPATIONS);
	}

	@Override
	public ActionListener getActionListenerForActionSaveModel() {
		return new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				actionSaveModel();
				try {
					actionFilterModelList();
				} catch (Exception e) {
					appFrame.showUserMessage(e, null);
				}
			}
		};
	}
	
	public ActionListener getActionListenerForActionShowCompetition() {
		return new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				actionShowCompetition();
			}
		};
	}
	
	public ActionListener getActionListenerForActionShowTeam() {
	    return new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			actionShowTeam();
		}
	    };
	}
	
	public ActionListener getActionListenerForActionShowCompetitionInstance() {
		return new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				actionShowCompetitionInstance();
			}
		};
	}
	
	public ActionListener getActionListenerForActionShowContact() {
		return new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				actionShowContact();
			}
		};
	}
        
        public ActionListener getActionListenerForActionResetFilters() {
            return (ActionEvent e) -> {
                resetFilters();
            };
        }
	
	public ActionListener getActionListenerForActionFilterModelList() {
		return new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				actionFilterModelList();
			}
		};
	}

	@Override
	protected void addFunctionsToMenuAndButtonPanel() {

		addFunctionToMenuAndButtonPanel(IconDictionary.getImageIcon(IconDictionary.CERTIFICATION_MF), Messages.getString(Messages.REPORT_CERTIFICATION_MF), getActionListenerForActionReportCertificationMf(), false);
		addFunctionToMenuAndButtonPanel(IconDictionary.getImageIcon(IconDictionary.CERTIFICATION_MF_AC), Messages.getString(Messages.REPORT_CERTIFICATION_MF_AC), getActionListenerForActionReportCertificationMfAc(), false);
		addFunctionToMenuAndButtonPanel(IconDictionary.getImageIcon(IconDictionary.CERTIFICATION_KIDS), Messages.getString(Messages.REPORT_CERTIFICATION_KIDS), getActionListenerForActionReportCertificationKids(), false);
		
		jcb_competition = new JComboBox();
		jp_buttonsRow2.add(jcb_competition);
		jcb_discipline = new JComboBox();
		jp_buttonsRow2.add(jcb_discipline);
		jcb_contact = new JComboBox();
		jp_buttonsRow2.add(jcb_contact);
		jcb_team = new JComboBox();
		jp_buttonsRow2.add(jcb_team);
                jcx_includeNonCompetitive = new JCheckBox();
                jcx_includeNonCompetitive.setToolTipText(Messages.getString(Messages.NONCOMPETITIVE));
                jp_buttonsRow2.add(jcx_includeNonCompetitive);
                jcx_includeCanceled = new JCheckBox();
                jcx_includeCanceled.setToolTipText(Messages.getString(Messages.CANCELED));
                jp_buttonsRow2.add(jcx_includeCanceled);
                jcx_includeNotStarted = new JCheckBox();
                jcx_includeNotStarted.setToolTipText(Messages.getString(Messages.NOT_STARTED));
                jp_buttonsRow2.add(jcx_includeNotStarted);
		addFunctionToMenuAndButtonPanel(
				IconDictionary.getImageIcon(IconDictionary.FILTER), 
				Messages.getString(Messages.FILTER_LIST), getActionListenerForActionFilterModelList(), true);
                addFunctionToMenuAndButtonPanel(
				IconDictionary.getImageIcon(IconDictionary.RESET_FILTERS), 
				Messages.getString(Messages.RESET_FILTERS), getActionListenerForActionResetFilters(), true);
		
		addSeparatorToMenuAndButtonPanel(false);
		
		addFunctionToMenuAndButtonPanelForActionNewModel();
		addFunctionToMenuAndButtonPanelForActionRemoveModel();
		addFunctionToMenuAndButtonPanelForActionSaveModel();
		
		addSeparatorToMenuAndButtonPanel(true);
		
		addFunctionToMenuAndButtonPanel(
				IconDictionary.getImageIcon(IconDictionary.CONTACT), 
				Messages.getString(Messages.SHOW_CONTACT), getActionListenerForActionShowContact(), true);
		addFunctionToMenuAndButtonPanel(
			IconDictionary.getImageIcon(IconDictionary.TEAM),
			Messages.getString(Messages.SHOW_TEAM), getActionListenerForActionShowTeam(), true);
		addFunctionToMenuAndButtonPanel(
				IconDictionary.getImageIcon(IconDictionary.DISCIPLINE), 
				Messages.getString(Messages.SHOW_DISCIPLINE), getActionListenerForActionShowDisciplineForSelectedParticipation(), true);
		addFunctionToMenuAndButtonPanel(
				IconDictionary.getImageIcon(IconDictionary.COMPETITION), 
				Messages.getString(Messages.SHOW_COMPETITION), getActionListenerForActionShowCompetition(), true);
		addFunctionToMenuAndButtonPanel(
				IconDictionary.getImageIcon(IconDictionary.COMPETITION_GRID), 
				Messages.getString(Messages.SHOW_COMPETITION_GRID), getActionListenerForActionShowCompetitionInstance(), true);
	}
	
	protected void actionShowDisciplineForSelectedParticipation() {
		Object o = jp_modelList.getSelectedModel();
		if (o != null && o instanceof Participation) {
			Participation p = (Participation) o;
			if (p.getDisciplineId() != 0) {
				appFrame.showDisciplineById(p.getDisciplineId());
			} else {
				appFrame.showUserMessage(JOptionPane.WARNING_MESSAGE, Messages.getString(Messages.NO_DISCIPLINE_RELATED), null);
			}
		} else {
			appFrame.showUserMessage(JOptionPane.WARNING_MESSAGE, Messages.getString(Messages.NO_PARTICIPATION_SELECTED_SELECT_ONE_FIRST), null);
		}
	}
	
	private ActionListener getActionListenerForActionShowDisciplineForSelectedParticipation() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				actionShowDisciplineForSelectedParticipation();
			}
		};
	}
	
	private ActionListener getActionListenerForActionReportCertificationMfAc() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				actionReportCertification();
			}
		};
	}
	
        private ActionListener getActionListenerForActionReportCertificationMf() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
                            actionReportCertificationMw();
			}
		};
	}
        
        private ActionListener getActionListenerForActionReportCertificationKids() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
                            actionReportCertificationKids();
			}
		};
	}
        
	private void actionReportCertification() {
		UIHelper.startWaiting(appFrame);
		Model[] models = jp_modelList.getSelectedModels();
		if (models != null && models.length > 0) {
			Participation[] participations = new Participation[models.length];
			int modelNo = 0;
			for (Model model: models) {
				if (model instanceof Participation) {
					participations[modelNo] = (Participation) model;
					modelNo++;
				}
			}
			try {
				RichParticipation[] richParticipations = controller.getRichParticipationsForParticipations(participations);
				if (richParticipations != null && richParticipations.length != 0) {
					Report[] reports = new Report[richParticipations.length];
					int reportNo = 0;
                                        Discipline discipline = null;
					for (RichParticipation richP: richParticipations) {
                                            if (richP != null) {
                                                discipline = richP.getDiscipline();
                                            }
						Report report = ReportFactory.createCertificationReportMfAc(new RichParticipationReportable(new RichParticipation[]{richP}), richP.getParticipant(), discipline);
						reports[reportNo] = report;
						reportNo++;
					}
					ReportingController.report(reports, appFrame);
				}
			} catch (SQLException e) {
				appFrame.showUserMessage(e, participations[0]);
			}
		}
		UIHelper.stopWaiting(appFrame);
	}
	private void actionReportCertificationMw() {
		UIHelper.startWaiting(appFrame);
		Model[] models = jp_modelList.getSelectedModels();
		if (models != null && models.length > 0) {
			Participation[] participations = new Participation[models.length];
			int modelNo = 0;
			for (Model model: models) {
				if (model instanceof Participation) {
					participations[modelNo] = (Participation) model;
					modelNo++;
				}
			}
			try {
				RichParticipation[] richParticipations = controller.getRichParticipationsForParticipations(participations);
				if (richParticipations != null && richParticipations.length != 0) {
					Report[] reports = new Report[richParticipations.length];
					int reportNo = 0;
                                        Discipline discipline = null;
					for (RichParticipation richP: richParticipations) {
                                            if (richP != null) {
                                                discipline = richP.getDiscipline();
                                            }
						Report report = ReportFactory.createCertificationMfReport(new RichParticipationReportable(new RichParticipation[]{richP}), richP.getParticipant(), discipline);
						reports[reportNo] = report;
						reportNo++;
					}
					ReportingController.report(reports, appFrame);
				}
			} catch (SQLException e) {
				appFrame.showUserMessage(e, participations[0]);
			}
		}
		UIHelper.stopWaiting(appFrame);
	}
	private void actionReportCertificationKids() {
		UIHelper.startWaiting(appFrame);
		Model[] models = jp_modelList.getSelectedModels();
		if (models != null && models.length > 0) {
			Participation[] participations = new Participation[models.length];
			int modelNo = 0;
			for (Model model: models) {
				if (model instanceof Participation) {
					participations[modelNo] = (Participation) model;
					modelNo++;
				}
			}
			try {
				RichParticipation[] richParticipations = controller.getRichParticipationsForParticipations(participations);
				if (richParticipations != null && richParticipations.length != 0) {
					Report[] reports = new Report[richParticipations.length];
					int reportNo = 0;
                                        Discipline discipline = null;
					for (RichParticipation richP: richParticipations) {
                                            if (richP != null) {
                                                discipline = richP.getDiscipline();
                                            }
						Report report = ReportFactory.createCertificationKidsReport(new RichParticipationReportable(new RichParticipation[]{richP}), richP.getParticipant(), discipline);
						reports[reportNo] = report;
						reportNo++;
					}
					ReportingController.report(reports, appFrame);
				}
			} catch (SQLException e) {
				appFrame.showUserMessage(e, participations[0]);
			}
		}
		UIHelper.stopWaiting(appFrame);
	}
        
        @Override
	public void actionReportModelList() {
		UIHelper.startWaiting(appFrame);
                try {
		RichParticipationReportable reportable = new RichParticipationReportable(controller.getRichParticipationsForParticipations(model.getModel()), getPanelTitle());
		ReportingController.report(ReportFactory.createParticipationListReport(reportable), appFrame);
                } catch (SQLException e) {
                    appFrame.showUserMessage(e, null);
                } finally {
                    UIHelper.stopWaiting(appFrame);
                }
	}

	@Override
	public JPanel getSpecificStatisticsPanel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Icon getIcon() {
		return IconDictionary.getImageIcon(IconDictionary.PARTICIPATIONS);
	}

}
