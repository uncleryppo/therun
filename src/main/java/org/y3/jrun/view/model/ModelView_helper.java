package org.y3.jrun.view.model;

import javax.swing.JComboBox;
import org.y3.jrun.model.Model;
import org.y3.jrun.model.ModelListModel;

/**
 * Copyright: 2011 - 2018
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class ModelView_helper {

	public static void setSelectedModelInComboBox(JComboBox comboBox, Model model) {
		if (comboBox != null) {
			if (model != null && comboBox.getModel() instanceof ModelListModel) {
				((ModelListModel) comboBox.getModel()).setSelectedModelByID(model.getId());
			} else {
                            comboBox.setSelectedItem(null);
                        }
		}
	}
	
}
