package org.y3.jrun.view.model.disciplinetimeschedule;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.y3.jrun.model.Model;
import org.y3.jrun.model.disciplinetimeschedule.DisciplineTimeSchedule;
import org.y3.jrun.view.i18n.Messages;
import org.y3.jrun.view.model.ModelForm;

/**
 * @author Ryppo
 *
 */
public class DisciplineTimeScheduleForm extends ModelForm {
    
    private static final long serialVersionUID = 1L;
    private DisciplineTimeSchedule model;
    private JTextField textField_title;
    private JTextField textField_start;
    private JTextField textField_end;
    private JComboBox comboBox_discipline;

    /**
     * @param mode
     */
    public DisciplineTimeScheduleForm(MODE mode) {
	super(mode);
    }

    @Override
    protected void initForm() {
	GridBagLayout gridBagLayout = new GridBagLayout();
	gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0};
	gridBagLayout.rowHeights = new int[]{0, 0, 0};
	gridBagLayout.columnWeights = new double[]{0.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
	gridBagLayout.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
	setLayout(gridBagLayout);
	
	JLabel label_title = new JLabel(Messages.getString(Messages.DISCIPLINE_TIME_SCHEDULE_TITLE));
	GridBagConstraints gbc_label_title = new GridBagConstraints();
	gbc_label_title.insets = new Insets(0, 0, 5, 5);
	gbc_label_title.anchor = GridBagConstraints.EAST;
	gbc_label_title.gridx = 0;
	gbc_label_title.gridy = 0;
	add(label_title, gbc_label_title);
	
	textField_title = new JTextField();
	GridBagConstraints gbc_textField_title = new GridBagConstraints();
	gbc_textField_title.insets = new Insets(0, 0, 5, 5);
	gbc_textField_title.fill = GridBagConstraints.HORIZONTAL;
	gbc_textField_title.gridx = 1;
	gbc_textField_title.gridy = 0;
	add(textField_title, gbc_textField_title);
	textField_title.setColumns(10);
	
	JLabel label_discipline = new JLabel(Messages.getString(Messages.DISCIPLINE));
	GridBagConstraints gbc_label_discipline = new GridBagConstraints();
	gbc_label_discipline.insets = new Insets(0, 0, 5, 5);
	gbc_label_discipline.anchor = GridBagConstraints.EAST;
	gbc_label_discipline.gridx = 2;
	gbc_label_discipline.gridy = 0;
	add(label_discipline, gbc_label_discipline);
	
	comboBox_discipline = new JComboBox();
	GridBagConstraints gbc_comboBox_discipline = new GridBagConstraints();
	gbc_comboBox_discipline.insets = new Insets(0, 0, 5, 0);
	gbc_comboBox_discipline.fill = GridBagConstraints.HORIZONTAL;
	gbc_comboBox_discipline.gridx = 3;
	gbc_comboBox_discipline.gridy = 0;
	add(comboBox_discipline, gbc_comboBox_discipline);
	
	JLabel label_start = new JLabel(Messages.getString(Messages.DISCIPLINE_TIME_SCHEDULE_START));
	GridBagConstraints gbc_label_start = new GridBagConstraints();
	gbc_label_start.anchor = GridBagConstraints.EAST;
	gbc_label_start.insets = new Insets(0, 0, 0, 5);
	gbc_label_start.gridx = 0;
	gbc_label_start.gridy = 1;
	add(label_start, gbc_label_start);
	
	textField_start = new JTextField();
	GridBagConstraints gbc_textField_start = new GridBagConstraints();
	gbc_textField_start.insets = new Insets(0, 0, 0, 5);
	gbc_textField_start.fill = GridBagConstraints.HORIZONTAL;
	gbc_textField_start.gridx = 1;
	gbc_textField_start.gridy = 1;
	add(textField_start, gbc_textField_start);
	textField_start.setColumns(10);
	
	JLabel label_end = new JLabel(Messages.getString(Messages.DISCIPLINE_TIME_SCHEDULE_END));
	GridBagConstraints gbc_label_end = new GridBagConstraints();
	gbc_label_end.insets = new Insets(0, 0, 0, 5);
	gbc_label_end.anchor = GridBagConstraints.EAST;
	gbc_label_end.gridx = 2;
	gbc_label_end.gridy = 1;
	add(label_end, gbc_label_end);
	
	textField_end = new JTextField();
	GridBagConstraints gbc_textField_end = new GridBagConstraints();
	gbc_textField_end.fill = GridBagConstraints.HORIZONTAL;
	gbc_textField_end.gridx = 3;
	gbc_textField_end.gridy = 1;
	add(textField_end, gbc_textField_end);
	textField_end.setColumns(10);
    }

    @Override
    public void bindData() {
    }

    @Override
    protected void switchFormToEditMode() {
	textField_title.setEditable(true);
	textField_start.setEditable(true);
	textField_end.setEditable(true);
	comboBox_discipline.setEnabled(true);
    }

    @Override
    protected void switchFormToAdministrationMode() {
	switchFormToEditMode();
    }

    @Override
    protected void switchFormToViewMode() {
	textField_title.setEditable(false);
	textField_start.setEditable(false);
	textField_end.setEditable(false);
	comboBox_discipline.setEnabled(false);
    }

    @Override
    public void setModel(Model _model) {
	if (_model != null && _model instanceof DisciplineTimeSchedule) {
	    model = (DisciplineTimeSchedule) _model;
//	    textField_title.set
//	    textField_start.set
//	    textField_end.set
//	    comboBox_discipline.set
	} else {
	    model = null;
	    textField_title.setText("");
	    textField_start.setText("");
	    textField_end.setText("");
	    comboBox_discipline.setSelectedItem(null);
	}
    }

    @Override
    public DisciplineTimeSchedule getModel() {
	if (model == null) {
	    model = new DisciplineTimeSchedule();
	}
	model.setTitle(textField_title.getText());
	//start
	//end
	//disci
	return model;
    }

    @Override
    public JComponent getFirstFocusableEditorComponent() {
	return textField_title;
    }

}
