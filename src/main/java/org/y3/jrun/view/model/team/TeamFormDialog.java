package org.y3.jrun.view.model.team;

import org.y3.jrun.control.ApplicationController;
import org.y3.jrun.model.team.Team;
import org.y3.jrun.view.ApplicationFrame;
import org.y3.jrun.view.gfx.IconDictionary;
import org.y3.jrun.view.model.ModelForm;
import org.y3.jrun.view.model.ModelFormDialog;

/**
 * Copyright: 2011 - 2018
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class TeamFormDialog extends ModelFormDialog {

    private static final long serialVersionUID = 1L;

    @Override
    public Integer getFormWidthHint() {
        return 500;
    }

    public TeamFormDialog(Team _model,
            ApplicationFrame applicationFrame,
            ApplicationController applicationController) {
        super(_model, applicationFrame, applicationController);
        setIconImage(IconDictionary.getImageIcon(IconDictionary.TEAM).getImage());
    }

    @Override
    public ModelForm getModelForm() {
        modelForm = new TeamForm(ModelForm.MODE.EDITOR);
        return modelForm;
    }

    @Override
    public boolean actionSave() {
        Team team = (Team) modelForm.getModel();
        try {
            controller.saveModel(team);
            return true;
        } catch (Exception e) {
            appFrame.showUserMessage(e, team);
        }
        return false;
    }

    @Override
    public Team getModel() {
        if (modelForm != null) {
            return (Team) modelForm.getModel();
        } else {
            return null;
        }
    }

}
