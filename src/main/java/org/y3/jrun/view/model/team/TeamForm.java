/**
 * 
 */
package org.y3.jrun.view.model.team;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.y3.jrun.model.Model;
import org.y3.jrun.model.team.Team;
import org.y3.jrun.view.i18n.Messages;
import org.y3.jrun.view.model.ModelForm;

/**
 * @author Ryppo
 *
 */
public class TeamForm extends ModelForm {
    
    /**
     * @param mode
     */
    public TeamForm(MODE mode) {
	super(mode);
    }

    private static final long serialVersionUID = 1L;
    private Team model;
    private JLabel labelValue_ID;
    private JTextField textfield_name;
    private final String dummy = "";

    /* (non-Javadoc)
     * @see org.y3.jrun.view.model.ModelForm#initForm()
     */
    @Override
    protected void initForm() {
	GridBagLayout gridBagLayout = new GridBagLayout();
	gridBagLayout.columnWidths = new int[]{0, 0, 0};
	gridBagLayout.rowHeights = new int[]{0, 0, 0};
	gridBagLayout.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
	gridBagLayout.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
	setLayout(gridBagLayout);
	
	JLabel label_ID = new JLabel(Messages.getString(Messages.MODEL_ID));
	label_ID.setFont(new Font("Lucida Grande", Font.BOLD, 13));
	label_ID.setForeground(Color.GRAY);
	GridBagConstraints gbc_label_ID = new GridBagConstraints();
	gbc_label_ID.anchor = GridBagConstraints.EAST;
	gbc_label_ID.insets = new Insets(0, 0, 5, 5);
	gbc_label_ID.gridx = 0;
	gbc_label_ID.gridy = 0;
	add(label_ID, gbc_label_ID);
	
	labelValue_ID = new JLabel();
	labelValue_ID.setForeground(Color.GRAY);
	labelValue_ID.setFont(new Font("Lucida Grande", Font.BOLD, 13));
	GridBagConstraints gbc_textfield_ID = new GridBagConstraints();
	gbc_textfield_ID.insets = new Insets(0, 0, 5, 0);
	gbc_textfield_ID.fill = GridBagConstraints.HORIZONTAL;
	gbc_textfield_ID.gridx = 1;
	gbc_textfield_ID.gridy = 0;
	add(labelValue_ID, gbc_textfield_ID);
	
	JLabel label_name = new JLabel(Messages.getString(Messages.NAME));
	GridBagConstraints gbc_label_name = new GridBagConstraints();
	gbc_label_name.anchor = GridBagConstraints.EAST;
	gbc_label_name.insets = new Insets(0, 0, 0, 5);
	gbc_label_name.gridx = 0;
	gbc_label_name.gridy = 1;
	add(label_name, gbc_label_name);
	
	textfield_name = new JTextField();
	GridBagConstraints gbc_textfield_name = new GridBagConstraints();
	gbc_textfield_name.fill = GridBagConstraints.HORIZONTAL;
	gbc_textfield_name.gridx = 1;
	gbc_textfield_name.gridy = 1;
	add(textfield_name, gbc_textfield_name);
	textfield_name.setColumns(10);
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.view.model.ModelForm#bindData()
     */
    @Override
    public void bindData() {
	// TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see org.y3.jrun.view.model.ModelForm#switchFormToEditMode()
     */
    @Override
    protected void switchFormToEditMode() {
	textfield_name.setEditable(true);
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.view.model.ModelForm#switchFormToAdministrationMode()
     */
    @Override
    protected void switchFormToAdministrationMode() {
	switchFormToEditMode();
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.view.model.ModelForm#switchFormToViewMode()
     */
    @Override
    protected void switchFormToViewMode() {
    	textfield_name.setEditable(false);
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.view.model.ModelForm#setModel(org.y3.jrun.model.Model)
     */
    @Override
    public void setModel(Model _model) {
	if (_model != null && _model instanceof Team) {
	    model = (Team) _model;
	    labelValue_ID.setText(Integer.toString(model.getId()));
	    textfield_name.setText(model.getName());
	} else {
	    model = null;
	    labelValue_ID.setText(dummy);
	    textfield_name.setText(dummy);
	}
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.view.model.ModelForm#getModel()
     */
    @Override
    public Model getModel() {
	if (model == null) {
	    model = new Team();
	}
	model.setName(textfield_name.getText());
	return model;
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.view.model.ModelForm#getFirstFocusableEditorComponent()
     */
    @Override
    public JComponent getFirstFocusableEditorComponent() {
	return textfield_name;
    }

}
