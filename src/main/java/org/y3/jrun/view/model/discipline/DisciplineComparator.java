package org.y3.jrun.view.model.discipline;

import java.util.Calendar;
import java.util.Comparator;
import java.util.GregorianCalendar;
import org.y3.jrun.model.discipline.Discipline;

/**
 * Copyright: 2011 - 2018
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class DisciplineComparator implements Comparator<Discipline> {

    public compareBy COMPARE_MODE = compareBy.NAME;
    
    public enum compareBy { NAME, LENGTH, SHORT_NAME, START_TIME }
    
    @Override
    public int compare(Discipline d1, Discipline d2) {
	if (d1 == null && d2 == null) {
	    return 0;
	} else if (d1 == null) {
	    return 1;
	} else if (d2 == null) {
	    return -1;
	} else {
	    String s1 = null;
	    String s2 = null;
            Calendar ds1 = null;
            Calendar ds2 = null;
	    switch (COMPARE_MODE) {
	    case NAME:
		s1 = d1.getName();
		s2 = d2.getName();
		break;
	    case SHORT_NAME:
		s1 = d1.getShortName();
		s2 = d2.getShortName();
		break;
	    case LENGTH:
		s1 = d1.getLength();
		s2 = d2.getLength();
		break;
            case START_TIME:
                ds1 = GregorianCalendar.getInstance();
		ds1.setTime(d1.getStartOfDiscipline());
                ds2 = GregorianCalendar.getInstance();
		ds2.setTime(d2.getStartOfDiscipline());
                break;
	    default:
		return 0;
	    }
            switch (COMPARE_MODE) {
                case START_TIME:
                    return ds1.compareTo(ds2);
                default:
                    return s1.compareTo(s2);
            }
	}
    }

}
