/**
 * 
 */
package org.y3.jrun.view.model.team;

import java.util.Comparator;

import org.y3.jrun.model.team.Team;

/**
 * @author Ryppo
 *
 */
public class TeamComparator implements Comparator<Team> {

    @Override
    public int compare(Team team1, Team team2) {
	//generic comparism upon null object(s)
	if (team1 == null && team2 == null) {
	    return 0;
	} else if (team1 == null) {
	    return 1;
	} else if (team2 == null) {
	    return -1;
	}
	//special mode based comparism
	String t1 = team1.getName();
	String t2 = team2.getName();
	return t1.compareTo(t2);
    }
    
}
