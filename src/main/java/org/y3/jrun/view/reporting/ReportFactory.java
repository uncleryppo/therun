package org.y3.jrun.view.reporting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRSortField;
import net.sf.jasperreports.engine.design.JRDesignSortField;
import net.sf.jasperreports.engine.type.SortFieldTypeEnum;
import net.sf.jasperreports.engine.type.SortOrderEnum;
import org.y3.jrun.control.KeywordsDictionary;
import org.y3.jrun.model.contact.Contact;
import org.y3.jrun.model.discipline.Discipline;
import org.y3.jrun.model.report.Report;
import org.y3.jrun.model.team.TeamRankingReportable;
import org.y3.jrun.view.i18n.Messages;

/**
 * Copyright: Since 2011
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class ReportFactory {

    public static Report createCompetitionListReport(JRDataSource reportableData) {
        Report report = new Report(
                Messages.getString(ReportsDictionary.COMPETITION_LIST),
                ReportsDictionary.COMPETITION_LIST, null, reportableData);
        return report;
    }

    public static Report createDonationReport(JRDataSource reportableData) {
        Report report = new Report(
                Messages.getString(ReportsDictionary.DONATION_REPORT),
                ReportsDictionary.DONATION_REPORT, null, reportableData);
        return report;
    }

    public static Report createParticipationConfirmationReport(JRDataSource reportableData,
                                                               Contact participant, Discipline discipline) {
        String extension = "";
        if (participant != null && participant.toString() != null && participant.toString().length() != 0) {
            extension += " - " + participant.toString();
        }
        if (discipline != null && discipline.toString() != null && discipline.toString().length() != 0) {
            extension += " - " + discipline.toString();
        }
        Report report = new Report(
                Messages.getString(ReportsDictionary.PARTICIPATION_CONFIRMATION_REPORT)+ extension,
                ReportsDictionary.PARTICIPATION_CONFIRMATION_REPORT, null, reportableData
        );
        return report;
    }

    public static Report createParticipationListReport(JRDataSource reportableData) {
        Report report = new Report(
                Messages.getString(ReportsDictionary.OVERVIEW_OF_PARTICIPATIONS),
                ReportsDictionary.OVERVIEW_OF_PARTICIPATIONS, null, reportableData
        );
        return report;
    }

    public static Report createModelListReport(JRDataSource reportableData) {
        Report report = new Report(
                Messages.getString(ReportsDictionary.MODEL_LIST),
                ReportsDictionary.MODEL_LIST, null, reportableData);
        return report;
    }

    public static Report createNotificationListReport(
            JRDataSource reportableData) {
        Report report = new Report(
                Messages.getString(ReportsDictionary.NOTIFICATION_LIST),
                ReportsDictionary.NOTIFICATION_LIST, null, reportableData);
        return report;
    }
    
    public static Report createParticipationsReport(JRDataSource reportableData, String title) {
        Map parameters = new HashMap();
        List<JRSortField> sortList = new ArrayList<>();
        JRDesignSortField sortField = new JRDesignSortField();
        sortField.setName(KeywordsDictionary.DISCIPLINE);
        sortField.setOrder(SortOrderEnum.ASCENDING);
        sortField.setType(SortFieldTypeEnum.FIELD);
        sortList.add(sortField);
        parameters.put(JRParameter.SORT_FIELDS, sortList);
        Report report = new Report(
                Messages.getString(ReportsDictionary.PARTICIPATIONS_LIST) + title,
                ReportsDictionary.PARTICIPATIONS_LIST, parameters, reportableData);
        return report;
    }
    
    public static Report createKidsStartGridReport(JRDataSource reportableData, Discipline discipline) {
    	String extension = "";
        if (discipline != null && discipline.toString() != null && discipline.toString().length() != 0) {
            extension += " - " + discipline.toString();
        }
        Map parameters = new HashMap();
        List<JRSortField> sortList = new ArrayList<>();
        JRDesignSortField sortField = new JRDesignSortField();
        sortField.setName(KeywordsDictionary.PARTICIPATION_RANK);
        sortField.setOrder(SortOrderEnum.ASCENDING);
        sortField.setType(SortFieldTypeEnum.FIELD);
        sortList.add(sortField);
        //parameters.put(JRParameter.SORT_FIELDS, sortList);
        Report report = new Report(
                Messages.getString(ReportsDictionary.DISCIPLINE_STARTGRID_KIDS) + extension,
                ReportsDictionary.DISCIPLINE_STARTGRID_KIDS, parameters, reportableData);
        return report;
    }

    public static Report createDisciplineStartGridReport(
            JRDataSource reportableData, Discipline discipline) {
        String extension = "";
        if (discipline != null && discipline.toString() != null && discipline.toString().length() != 0) {
            extension += " - " + discipline.toString();
        }
        Map parameters = new HashMap();
        List<JRSortField> sortList = new ArrayList<>();
        JRDesignSortField sortField = new JRDesignSortField();
        sortField.setName(KeywordsDictionary.PARTICIPATION_PARTICIPATION_NUMBER);
        sortField.setOrder(SortOrderEnum.ASCENDING);
        sortField.setType(SortFieldTypeEnum.FIELD);
        sortList.add(sortField);
        parameters.put(JRParameter.SORT_FIELDS, sortList);
        Report report = new Report(
                Messages.getString(ReportsDictionary.DISCIPLINE_STARTGRID) + extension,
                ReportsDictionary.DISCIPLINE_STARTGRID, parameters, reportableData);
        return report;
    }

    public static Report createDisciplineGenderStartGridReport(
            JRDataSource reportableData, HashMap<String, Object> parameters, Discipline discipline) {
        String extension = "";
        if (discipline != null && discipline.toString() != null && discipline.toString().length() != 0) {
            extension += " - " + discipline.toString();
        }
        Report report = new Report(
                Messages.getString(ReportsDictionary.DISCIPLINE_GENDER_STARTGRID) + extension,
                ReportsDictionary.DISCIPLINE_GENDER_STARTGRID, parameters, reportableData);
        return report;
    }

    public static Report createDisciplineAgeClassStartGridReport(
            JRDataSource reportableData, HashMap<String, Object> parameters, Discipline discipline) {
        String extension = "";
        if (discipline != null && discipline.toString() != null && discipline.toString().length() != 0) {
            extension += " - " + discipline.toString();
        }
        Report report = new Report(
                Messages.getString(ReportsDictionary.DISCIPLINE_AGECLASS_STARTGRID) + extension,
                ReportsDictionary.DISCIPLINE_AGECLASS_STARTGRID, parameters, reportableData, false, true);
        return report;
    }

    public static Report createDisciplineGenderAgeClassStartGridReport(
            JRDataSource reportableData, HashMap<String, Object> parameters, Discipline discipline) {
        String extension = "";
        if (discipline != null && discipline.toString() != null && discipline.toString().length() != 0) {
            extension += " - " + discipline.toString();
        }
        Report report = new Report(
                Messages.getString(ReportsDictionary.DISCIPLINE_GENDER_AGECLASS_STARTGRID) + extension,
                ReportsDictionary.DISCIPLINE_GENDER_AGECLASS_STARTGRID, parameters, reportableData, false, true);
        return report;
    }

    public static Report createTeamRankingReport(
            TeamRankingReportable reportableData, Discipline discipline) {
        String extension = "";
        if (discipline != null && discipline.toString() != null && discipline.toString().length() != 0) {
            extension += " - " + discipline.toString();
        }
        Report report = new Report(
                Messages.getString(ReportsDictionary.TEAM_RANKING) + extension,
                ReportsDictionary.TEAM_RANKING, null, reportableData);
        return report;
    }

    public static Report createCertificationReportMfAc(JRDataSource reportableData,
            Contact participant, Discipline discipline) {
        String extension = "";
        if (participant != null && participant.toString() != null && participant.toString().length() != 0) {
            extension += " - " + participant.toString();
        }
        if (discipline != null && discipline.toString() != null && discipline.toString().length() != 0) {
            extension += " - " + discipline.toString();
        }
        Report report = new Report(
                Messages.getString(ReportsDictionary.CERTIFICATION_MF_AC)
                + extension, ReportsDictionary.CERTIFICATION_MF_AC,
                null, reportableData);
        return report;
    }

    public static Report createCertificationMfReport(JRDataSource reportableData,
            Contact participant, Discipline discipline) {
        String extension = "";
        if (participant != null && participant.toString() != null && participant.toString().length() != 0) {
            extension += " - " + participant.toString();
        }
        if (discipline != null && discipline.toString() != null && discipline.toString().length() != 0) {
            extension += " - " + discipline.toString();
        }
        Report report = new Report(
                Messages.getString(ReportsDictionary.CERTIFICATION_MW)
                + extension, ReportsDictionary.CERTIFICATION_MW,
                null, reportableData);
        return report;
    }

    public static Report createCertificationKidsReport(JRDataSource reportableData,
            Contact participant, Discipline discipline) {
        String extension = "";
        if (participant != null && participant.toString() != null && participant.toString().length() != 0) {
            extension += " - " + participant.toString();
        }
        if (discipline != null && discipline.toString() != null && discipline.toString().length() != 0) {
            extension += " - " + discipline.toString();
        }
        Report report = new Report(
                Messages.getString(ReportsDictionary.CERTIFICATION_KIDS)
                + extension, ReportsDictionary.CERTIFICATION_KIDS,
                null, reportableData);
        return report;
    }

    public static Report createAgeClassesDefinitionReport(JRDataSource reportableData) {
        Report report = new Report(Messages.getString(ReportsDictionary.AGECLASSES_DEFINITION),
                ReportsDictionary.AGECLASSES_DEFINITION, null, reportableData);
        return report;
    }

}
