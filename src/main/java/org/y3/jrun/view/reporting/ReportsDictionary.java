package org.y3.jrun.view.reporting;

import java.io.InputStream;

/**
 * Copyright: Since 2011
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class ReportsDictionary {

	private static String location = "org/y3/jrun/view/reporting/reports/";
	
	public static InputStream getReport(String report) {
		return ReportsDictionary.class.getClassLoader().getResourceAsStream(location + report);
	}
	
	public static String COMPETITION_LIST 						= "competitionListReport.jasper";
	public static String DONATION_REPORT						= "donationReport.jasper";
	public static String PARTICIPATION_CONFIRMATION_REPORT		= "participationConfirmation.jasper";
	public static String OVERVIEW_OF_PARTICIPATIONS 			= "participationsReport.jasper";
	public static String MODEL_LIST 							= "modelListReport.jasper";
	public static String NOTIFICATION_LIST 						= "notificationListReport.jasper";
	public static String DISCIPLINE_STARTGRID 					= "disciplineStartGridReport.jasper";
	public static String DISCIPLINE_STARTGRID_KIDS 				= "disciplineStartGridKidsReport.jasper";
	public static String PARTICIPATIONS_LIST 					= "participationsListReport.jasper";
	public static String DISCIPLINE_GENDER_STARTGRID			= "disciplineGenderStartGridReport.jasper";
	public static String DISCIPLINE_AGECLASS_STARTGRID  		= "disciplineAgeClassStartGridReport.jasper";
	public static String DISCIPLINE_GENDER_AGECLASS_STARTGRID	= "disciplineGenderAgeClassStartGridReport.jasper";
	public static String CERTIFICATION_MF_AC					= "certification_mf_ac.jasper";
	public static String CERTIFICATION_MW						= "certification2_mf.jasper";
	public static String CERTIFICATION_KIDS						= "certification_kids.jasper";
	public static String AGECLASSES_DEFINITION					= "ageclassesDefinitionReport.jasper";
	public static String TEAM_RANKING                             = "teamRankingReport.jasper";
	
}
