package org.y3.jrun.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.y3.commons.swing.ApplicationInfoDialog;
import org.y3.jrun.control.ApplicationController;
import org.y3.jrun.model.database.DatabaseInformation;
import org.y3.jrun.view.gfx.IconDictionary;
import org.y3.jrun.view.i18n.Messages;

/**
 * Copyright: 2011 - 2018
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class ApplicationMenubar extends JMenuBar {
	
	private static Logger log = LogManager.getLogger();

	private static final long serialVersionUID = 1L;
	private ApplicationController controller;
	private ApplicationFrame appFrame;
	private JMenuItem 	jmi_openDatabase;
	private JMenuItem	jmi_setDatabaseAndSystemExit;
	private JMenuItem	jmi_createDatabase;
	private JMenuItem	jmi_removeDatabase;
	private JMenuItem	jmi_connectDatabase;
	private JMenuItem       jmi_showApplicationInfo;
        
	public ApplicationMenubar(ApplicationController _controller, ApplicationFrame _appFrame) {
		appFrame = _appFrame;
		controller = _controller;
		init();
	}
	
	private void init() {
            
		JMenu jmenu_file = new JMenu(Messages.getString(Messages.FILE));
		add(jmenu_file);
		
                jmi_showApplicationInfo = new JMenuItem(Messages.getString(Messages.APPLICATION_ABOUT));
                jmi_showApplicationInfo.setIcon(IconDictionary.getImageIcon(IconDictionary.INFO));
                jmenu_file.add(jmi_showApplicationInfo);
                jmi_showApplicationInfo.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent arg0) {
			actionShowApplicationInfoDialog();
		    }
		});
                
		jmi_openDatabase = new JMenuItem(Messages.getString(Messages.OPEN_DATABASE));
                jmi_openDatabase.setIcon(IconDictionary.getImageIcon(IconDictionary.DATABASE_OPEN));
		jmenu_file.add(jmi_openDatabase);
		jmi_openDatabase.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent arg0) {
			openDatabase(true);
		    }
		});
		
		jmi_setDatabaseAndSystemExit = new JMenuItem(Messages.getString(Messages.SET_DATABASE_AND_EXIT));
		jmi_setDatabaseAndSystemExit.setIcon(IconDictionary.getImageIcon(IconDictionary.DATABASE_SET_AND_EXIT));
		jmenu_file.add(jmi_setDatabaseAndSystemExit);
		jmi_setDatabaseAndSystemExit.addActionListener(new ActionListener() { 
		    @Override
		    public void actionPerformed(ActionEvent arg0) {
			if (openDatabase(false)) {
			    try {
				if (controller.shutDown()) {
				    System.exit(0);
				}
			    } catch (SQLException e) {
				appFrame.showUserMessage(e, null);
			    }
			} else {
			    log.debug("nein");
			}
		    }
		});

		jmi_createDatabase = new JMenuItem(Messages.getString(Messages.CREATE_DATABASE));
		jmi_createDatabase.setIcon(IconDictionary.getImageIcon(IconDictionary.DATABASE_ADD));
		jmenu_file.add(jmi_createDatabase);
		jmi_createDatabase.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
		    JFileChooser fc = new JFileChooser();
		    int returnVal = fc.showSaveDialog(appFrame);
		    if (returnVal == JFileChooser.APPROVE_OPTION) {
			DatabaseInformation dbi = new DatabaseInformation();
			dbi.setHomeLocation(fc.getSelectedFile()
				.getAbsolutePath());
			controller.setDatabaseInformation(dbi);
			if (controller.isDatabaseOnline()) {
			    controller.disconnectDatabase();
			}
			boolean created = controller.createDatabase();
			appFrame.setOnline(created);
			if (created) {
			    controller.connectDatabase(dbi);
			    appFrame.showUserMessage(
				    JOptionPane.INFORMATION_MESSAGE,
				    Messages.getString(Messages.DATABASE_SUCCESSFULLY_CREATED),
				    null);
			} else {
			    appFrame.showUserMessage(
				    JOptionPane.WARNING_MESSAGE,
				    Messages.getString(Messages.DATABASE_NOT_CREATED),
				    null);
			}
		    }
		} catch (Exception e1) {
		    appFrame.showUserMessage(e1, null);
		}
	    }
	});
		
		jmi_removeDatabase = new JMenuItem(Messages.getString(Messages.REMOVE_DATABASE));
		jmi_removeDatabase.setIcon(IconDictionary.getImageIcon(IconDictionary.DATABASE_REMOVE));
		jmenu_file.add(jmi_removeDatabase);
		jmi_removeDatabase.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					boolean removed = controller.removedDatabase();
					appFrame.setOnline(!removed);
					if (removed) {
						appFrame.showUserMessage(JOptionPane.INFORMATION_MESSAGE, 
								Messages.getString(Messages.DATABASE_SUCCESSFULLY_REMOVED), null);
					} else {
						appFrame.showUserMessage(JOptionPane.WARNING_MESSAGE, 
								Messages.getString(Messages.DATABASE_NOT_REMOVED), null);
					}
				} catch (Exception e1) {
					appFrame.showUserMessage(e1, null);
				}
				appFrame.bindData();
			}
		});
		
		JSeparator separator = new JSeparator();
		jmenu_file.add(separator);
		
		jmi_connectDatabase = new JMenuItem(Messages.getString(Messages.RE_CONNECT_TO_DATABASE));
		jmi_connectDatabase.setIcon(IconDictionary.getImageIcon(IconDictionary.CONNECT));
		jmenu_file.add(jmi_connectDatabase);
		jmi_connectDatabase.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					DatabaseInformation databaseInformation = controller.getDatabaseInformation();
					boolean connected = controller.connectDatabase(databaseInformation);
					appFrame.setOnline(connected);
					if (connected) {
						appFrame.bindData();
						appFrame.showUserMessage(JOptionPane.INFORMATION_MESSAGE, 
								Messages.getString(Messages.DATABASE_SUCCESSFULLY_CONNECTED), null);
					} else {
						appFrame.showUserMessage(JOptionPane.WARNING_MESSAGE, 
								Messages.getString(Messages.DATABASE_NOT_CONNECTED), null);
					}
				} catch (Exception e1) {
					appFrame.showUserMessage(e1, null);
				}
			}
		});
		
		JMenuItem jmi_shutDown = new JMenuItem(Messages.getString(Messages.SHUT_DOWN));
		jmi_shutDown.setIcon(IconDictionary.getImageIcon(IconDictionary.SHUT_DOWN));
		jmenu_file.add(jmi_shutDown);
		jmi_shutDown.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				appFrame.actionShutDown();
			}
		});
	}
	
	/**
	 * Set menu bar online. If true, menu items, which need a database connections are enabled.
	 * @param online database is online (available & connected)
	 */
	public void setOnline(boolean online) {
		jmi_removeDatabase.setEnabled(online);
		jmi_connectDatabase.setEnabled(online);
	}
        
        private void actionShowApplicationInfoDialog() {
            ApplicationInfoDialog applicationInfoDialog =
                            new ApplicationInfoDialog(
                                    appFrame,
                                    Messages.getString(Messages.APPLICATION_NAME),
                                    Messages.getString(Messages.APPLICATION_VERSION),
                                    new String[]{"Christian Rybotycky"},
                                    IconDictionary.getImageIcon(IconDictionary.DISCIPLINE).getImage(),
                                    IconDictionary.getImageIconApp(IconDictionary.APP_SPLASH));
                    applicationInfoDialog.setVisible(true);
        }


	/**
	 * Opens a database
	 * @param bind if true, binds to database
	 * @return ture, if database is opened
	 */
	public boolean openDatabase(boolean bind) {
	    try {
	    JFileChooser fc = new JFileChooser();
	    fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    int returnVal = fc.showOpenDialog(appFrame);
	    if (returnVal == JFileChooser.APPROVE_OPTION) {
		controller.disconnectDatabase();
		DatabaseInformation dbi = new DatabaseInformation();
		dbi.setHomeLocation(fc.getSelectedFile().getAbsolutePath());
		if (controller.setDatabaseInformation(dbi)
			&& controller.connectDatabase(dbi)) {
		    log.debug("Verbunden mit " + dbi.getHomeLocation());
		    if (bind) {
			appFrame.bindData();
		    }
		} else {
		    log.debug("Nicht verbunden mit "
			    + dbi.getHomeLocation());
		}
		return true;
	    }
	    } catch (Exception e) {
	        appFrame.showUserMessage(e, null);
	    }
	    return false;
	}
	
}
