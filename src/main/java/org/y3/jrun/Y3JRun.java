package org.y3.jrun;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.y3.commons.swing.ApplicationInfoDialog;
import org.y3.jrun.control.ApplicationController;
import org.y3.jrun.model.database.DatabaseInformation;
import org.y3.jrun.view.ApplicationFrame;
import org.y3.jrun.view.gfx.IconDictionary;
import org.y3.jrun.view.i18n.Messages;

import it.sauronsoftware.junique.AlreadyLockedException;
import it.sauronsoftware.junique.JUnique;
import net.sf.jasperreports.engine.JRPropertiesUtil;

/**
 * Copyright: 2011 - 2018
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class Y3JRun {

	private static Logger log = LogManager.getLogger();

    private static boolean DEBUG = true;
    private static boolean DEBUG_CONSOLE = false;
    private static boolean SEBN_PROXY = false;

    private ApplicationFrame appFrame;

    private static final String appId = "Y3JRun";

    private void actionShowApplicationInfoDialog() {
        ApplicationInfoDialog applicationInfoDialog
                = new ApplicationInfoDialog(
                        appFrame,
                        Messages.getString(Messages.APPLICATION_NAME),
                        Messages.getString(Messages.APPLICATION_VERSION),
                        new String[]{"Christian Rybotycky"},
                        IconDictionary.getImageIcon(IconDictionary.DISCIPLINE).getImage(),
                        IconDictionary.getImageIconApp(IconDictionary.APP_SPLASH));
        applicationInfoDialog.setVisible(true);
    }

    /**
     * Constructor
     */
    public Y3JRun() {
        String osName = System.getProperty("os.name").toLowerCase();
        boolean isMacOs = osName.startsWith("mac os x");
        log.debug("macos: " + isMacOs);
        if (isMacOs) {
            //osx features
            System.setProperty("apple.awt.graphics.EnableQ2DX", "true");
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Y3JRun");
            System.setProperty("apple.laf.useScreenMenuBar", "true");
            com.apple.eawt.Application.getApplication().setDockIconBadge(appId);
            com.apple.eawt.Application.getApplication().setDockIconImage(IconDictionary.getImageIconApp(IconDictionary.APP_SPLASH).getImage());
        }
//                com.apple.eawt.Application.getApplication().setAboutHandler(new AboutHandler() {
//
//                @Override
//                public void handleAbout(AppEvent.AboutEvent ae) {
//                    actionShowApplicationInfoDialog();
//                }
//            });

        //proxy features
        if (SEBN_PROXY) {
            System.getProperties().put("http.proxyHost", "130.30.1.130");
            System.getProperties().put("http.proxyPort", "8080");
        }
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        //windows features
        String defaultPDFFont = "Lucida Sans Typewriter";
        JRPropertiesUtil util = JRPropertiesUtil.getInstance(DefaultJasperReportsContext.getInstance());
        util.setProperty("net.sf.jasperreports.awt.ignore.missing.font", "true");
        util.setProperty("net.sf.jasperreports.default.font.name", defaultPDFFont);
    }

    /**
     * Runs Y3JavaRun
     */
    public void run(boolean debug, boolean debugConsole) {
        ApplicationController appControl = new ApplicationController(debug);
        //RestService restService = new RestService(appControl);
        appFrame = new ApplicationFrame(appControl, debugConsole);
        appFrame.showDebugConsole(true);
        appFrame.setVisible(true);
        try {
            DatabaseInformation databaseInformation = appControl.getDatabaseInformation();
            boolean connected = appControl.connectDatabase(databaseInformation);
            if (connected) {
                appFrame.bindData();
            }
        } catch (Exception e) {
            appFrame.showUserMessage(e, null);
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        //check no other instance is already running
        boolean runnable = false;
        try {
            JUnique.acquireLock(appId);
            runnable = true;
        } catch (AlreadyLockedException e) {
            runnable = false;
            log.error("Another instance of this application is already running.");
        }
        //run application
        if (runnable) {
            parseArgs(args);
            Y3JRun y3JavaRun = new Y3JRun();
            y3JavaRun.run(DEBUG, DEBUG_CONSOLE);
        }

    }

    public static void parseArgs(String[] args) {
        if (args != null && args.length > 0) {
            DEBUG = Boolean.parseBoolean(args[0]);
            if (args.length > 1) {
                DEBUG_CONSOLE = Boolean.parseBoolean(args[1]);
            }
            if (args.length > 2) {
                SEBN_PROXY = Boolean.parseBoolean(args[2]);
            }
        }
    }

}
