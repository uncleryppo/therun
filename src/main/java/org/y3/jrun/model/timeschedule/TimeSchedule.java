package org.y3.jrun.model.timeschedule;

import org.y3.jrun.model.Model;

public class TimeSchedule extends Model {
    
    private String name;
    private String note;
    
    /* (non-Javadoc)
     * @see org.y3.jrun.model.Model#toString()
     */
    @Override
    public String toString() {
	if (name == null) {
		return "";
	}
	return name;
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.model.Model#generateImportFingerPrint()
     */
    @Override
    public void generateImportFingerPrint() {
	setImportFingerPrint(toString());
    }

    /**
     * @return the name
     */
    public String getName() {
	return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
	this.name = name;
    }

    /**
     * @return the note
     */
    public String getNote() {
	return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
	this.note = note;
    }

}
