/**
 * 
 */
package org.y3.jrun.model.timeschedule;

import org.y3.jrun.model.ModelListModel;

/**
 * @author Ryppo
 * 
 */
public class TimeScheduleListModel extends ModelListModel {

    private static final long serialVersionUID = 1L;

    /**
     * @param models
     */
    public TimeScheduleListModel(TimeSchedule[] models) {
	super(models);
    }

    @Override
    public TimeSchedule getElementAt(int arg0) {
	return (TimeSchedule) super.getElementAt(arg0);
    }

    @Override
    public TimeSchedule[] getModel() {
	return (TimeSchedule[]) super.getModel();
    }

}
