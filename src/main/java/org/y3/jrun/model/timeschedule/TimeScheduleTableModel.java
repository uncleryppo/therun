/**
 * 
 */
package org.y3.jrun.model.timeschedule;

import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

/**
 * @author Ryppo
 * 
 */
public class TimeScheduleTableModel extends DefaultTableModel {

    private static final long serialVersionUID = 1L;

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public Class getColumnClass(int columnIndex) {
	return TimeSchedule.class;
    }

    public TimeSchedule[] getModel() {
	Object objects[] = super.dataVector.toArray();
	ArrayList<TimeSchedule> timeSchedule = new ArrayList<TimeSchedule>(0);
	for (Object o : objects) {
	    if (o instanceof TimeSchedule) {
		timeSchedule.add((TimeSchedule) o);
	    }
	}
	if (timeSchedule.size() != 0) {
	    return (TimeSchedule[]) timeSchedule.toArray();
	} else {
	    return null;
	}
    }
}
