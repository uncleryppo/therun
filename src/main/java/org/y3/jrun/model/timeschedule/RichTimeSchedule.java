package org.y3.jrun.model.timeschedule;

import org.y3.jrun.model.disciplinetimeschedule.DisciplineTimeSchedule;

/**
 * @author Ryppo
 *
 */
public class RichTimeSchedule extends TimeSchedule {

    private DisciplineTimeSchedule[] disciplineTimeSchedules;

    public RichTimeSchedule(TimeSchedule _timeSchedule, DisciplineTimeSchedule[] _disciplineTimeSchedules) {
	fillRichTimeScheduleByTimeSchedule(_timeSchedule);
	disciplineTimeSchedules = _disciplineTimeSchedules;
    }
    
    public void fillRichTimeScheduleByTimeSchedule(TimeSchedule _timeSchedule) {
	if (_timeSchedule != null) {
	    setName(_timeSchedule.getName());
	    setNote(_timeSchedule.getNote());
	    setCreatorName(_timeSchedule.getCreatorName());
	    setCreationDate(_timeSchedule.getCreationDate());
	    setChangerName(_timeSchedule.getChangerName());
	    setChangeDate(_timeSchedule.getChangeDate());
	    setId(_timeSchedule.getId());
	} else {
	    setName(null);
	    setNote(null);
	    setCreatorName(null);
	    setCreationDate(null);
	    setChangerName(null);
	    setChangeDate(null);
	    setId(0);
	}
    }
    
    /**
     * @return the disciplineTimeSchedules
     */
    public DisciplineTimeSchedule[] getDisciplineTimeSchedules() {
	return disciplineTimeSchedules;
    }

    /**
     * @param disciplineTimeSchedules the disciplineTimeSchedules to set
     */
    public void setDisciplineTimeSchedules(DisciplineTimeSchedule[] disciplineTimeSchedules) {
	this.disciplineTimeSchedules = disciplineTimeSchedules;
    }
}
