package org.y3.jrun.model.database;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.y3.jrun.control.KeywordsDictionary;
import org.y3.jrun.model.Model;

/**
 * Copyright: 2011 - 2018
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class DatabaseInformation extends Model {

    private String homeLocation = null;
    private Connection connection = null;

    public String getHomeLocation() {
        return homeLocation;
    }

    /**
     * Get connection string
     * @return connection string for database
     */
    public String getConnectionString() {
        return KeywordsDictionary.DATABASE_DERBY_CONNECTION_BRIDGE + homeLocation
                + ";user="
                + KeywordsDictionary.DATABASE_USERNAME + ";password="
                + KeywordsDictionary.DATABASE_PASSWORD;
    }

    /**
     * Create connection to database
     * @return database statement
     * @throws SQLException
     */
    public Statement createConnection() {
        Statement connectedStatement = null;
        try {
            connection = DriverManager.getConnection(getConnectionString() + ";"
                    //+ KeywordsDictionary.DATABASE_DERBY_PARAM_SCHEMA_EXISTS
                    + ";" + KeywordsDictionary.DATABASE_DERBY_PARAM_CREATE_NOT_DATABASE
            );
            connectedStatement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseInformation.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            return connectedStatement;
        }
    }

    public Statement createDatabaseCreator() throws SQLException {
        connection = DriverManager.getConnection(getConnectionString() + ";"
                + KeywordsDictionary.DATABASE_DERBY_PARAM_CREATE_DATABASE);
        return connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
    }

    /**
     * Checks if database is connected
     * @return true, if database is connected
     * @throws SQLException
     */
    public boolean isConnected() throws SQLException {
        if (connection != null && !connection.isClosed()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if database structure exists
     * @return true, if structure exists
     * @throws SQLException
     */
    public boolean structureExists() throws SQLException {
        Statement connectedStatement = null;
        if (connection == null) {
            connectedStatement = createConnection();
        }
        if (connectedStatement == null) {
            return false;
        } else {
            // check schema exists
            DatabaseMetaData dmd = connection.getMetaData();
            ResultSet schemas = dmd.getSchemas();
            while (schemas.next()) {
                String actualSchema = schemas.getString(1);
                if (actualSchema.toUpperCase().equals(
                        KeywordsDictionary.DATABASE_SCHEME.toUpperCase())) {
                    return true;
                }
            }
            return false;
        }
    }

    public void setHomeLocation(String homeLocation) {
        this.homeLocation = homeLocation;
    }


    /* 
	 * @see org.y3.jrun.model.Model#toString()
     */
    @Override
    public String toString() {
        return homeLocation;
    }


    /* 
	 * @see org.y3.jrun.model.Model#generateImportFingerPrint()
     */
    @Override
    public void generateImportFingerPrint() {
        setImportFingerPrint(toString());
    }

}
