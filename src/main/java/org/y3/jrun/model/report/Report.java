package org.y3.jrun.model.report;

import java.util.Map;
import net.sf.jasperreports.engine.JRDataSource;
import org.apache.commons.lang.StringUtils;
import org.y3.jrun.model.Model;
import org.y3.jrun.view.i18n.Messages;

public class Report extends Model {
	
	private String reportFile;
	private Map<String, Object> parameters;
	private JRDataSource reportableData;
	private String saveLocation;
	private String reportName;
	private boolean usePureParameterNameStringForName = false;
        private boolean usePureParameterValueStringForName = false;
	
	public Report(String _reportName, String _reportFile, Map<String, Object> _parameters, JRDataSource _reportableData) {
		reportName = _reportName;
		reportFile = _reportFile;
		parameters = _parameters;
		reportableData = _reportableData;
	}
	
	public Report(String _reportName, String _reportFile, 
                Map<String, Object> _parameters, JRDataSource _reportableData, 
                boolean _usePureParameterStringForName,
                boolean _usePureParameterValueStringForName) {
		reportName = _reportName;
		reportFile = _reportFile;
		parameters = _parameters;
		reportableData = _reportableData;
		usePureParameterNameStringForName = _usePureParameterStringForName;
                usePureParameterValueStringForName = _usePureParameterValueStringForName;
	}

	/**
	 * @return the reportFile
	 */
	public String getReportFile() {
		return reportFile;
	}

	/**
	 * @param reportFile the reportFile to set
	 */
	public void setReportFile(String reportFile) {
		this.reportFile = reportFile;
	}

	/**
	 * @return the parameters
	 */
	public Map<String, Object> getParameters() {
		return parameters;
	}

	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}

	/**
	 * @return the reportableData
	 */
	public JRDataSource getReportableData() {
		return reportableData;
	}

	/**
	 * @param reportableData the reportableData to set
	 */
	public void setReportableData(JRDataSource reportableData) {
		this.reportableData = reportableData;
	}

	@Override
	public String toString() {
		String toString;
		if (saveLocation != null && saveLocation.length() > 0) {
			toString = saveLocation;
		} else {
			toString = reportName;
		}
		if (parameters != null && !parameters.isEmpty()) {
                    for (Map.Entry<String, Object> parameter : parameters.entrySet()) {
                        String parameterName = parameter.getKey();
                        String parameterValue = parameter.getValue() instanceof String ? (String) parameter.getValue() : "";
                        String parameterText = "";
                        if (!usePureParameterNameStringForName && StringUtils.isNotEmpty(parameterName)) {
                            parameterName = Messages.getString(parameterName);
			}
                        if (!usePureParameterValueStringForName && StringUtils.isNotEmpty(parameterValue)) {
                            parameterValue = Messages.getString(parameterValue);
                        }
                        parameterText = StringUtils.isNotEmpty(parameterValue) ? parameterValue : parameterName;
                        if (StringUtils.isNotEmpty(parameterText)) {
                            toString+= " - " + parameterText;	
                        }
                    }
		}
		return toString;
	}
	
	@Override
	public String getNotificationTitle() {
		return getReportName();
	}
	
	@Override
	public String getNotificationMessage() {
		String value = "";
		if (parameters != null && !parameters.isEmpty()) {
			value = "parameters:\n";
			String paramValue = parameters.get(parameters.keySet().toArray()[0].toString()).toString();
			value+= " - " + paramValue;
		}
		return value;
	}

	@Override
	public void generateImportFingerPrint() {
	}

	public String getSaveLocation() {
		return saveLocation;
	}

	public void setSaveLocation(String saveLocation) {
		this.saveLocation = saveLocation;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

}
