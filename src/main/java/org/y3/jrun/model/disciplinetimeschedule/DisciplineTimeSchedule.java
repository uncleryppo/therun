package org.y3.jrun.model.disciplinetimeschedule;

import java.util.Calendar;
import java.util.Date;

import org.y3.jrun.model.Model;

public class DisciplineTimeSchedule extends Model {

    private String title;
    private int relatedDisciplineId;
    private Date startOfTimeSchedule;
    private Date endOfTimeSchedule;
    private int relatedTimeScheduleId;
	
    @Override
    public String toString() {
	String toString = title;
	if (startOfTimeSchedule != null) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(startOfTimeSchedule.getTime());
		toString += " " +  cal.get(Calendar.YEAR);
	}
	return toString;
    }

    @Override
    public void generateImportFingerPrint() {
	setImportFingerPrint(toString());
    }

    /**
     * @return the title
     */
    public String getTitle() {
	return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
	this.title = title;
    }

    /**
     * @return the relatedDisciplineId
     */
    public int getRelatedDisciplineId() {
	return relatedDisciplineId;
    }

    /**
     * @param relatedDisciplineId the relatedDisciplineId to set
     */
    public void setRelatedDisciplineId(int relatedDisciplineId) {
	this.relatedDisciplineId = relatedDisciplineId;
    }

    /**
     * @return the startOfTimeSchedule
     */
    public Date getStartOfTimeSchedule() {
	return startOfTimeSchedule;
    }

    /**
     * @param startOfTimeSchedule the startOfTimeSchedule to set
     */
    public void setStartOfTimeSchedule(Date startOfTimeSchedule) {
	this.startOfTimeSchedule = startOfTimeSchedule;
    }

    /**
     * @return the relatedTimeSchedule id
     */
    public int getRelatedTimeScheduleId() {
	return relatedTimeScheduleId;
    }

    /**
     * @param relatedTimeScheduleId the relatedTimeSchedule id to set
     */
    public void setRelatedTimeScheduleId(int relatedTimeScheduleId) {
	this.relatedTimeScheduleId = relatedTimeScheduleId;
    }

    /**
     * @return the endOfTimeSchedule
     */
    public Date getEndOfTimeSchedule() {
	return endOfTimeSchedule;
    }

    /**
     * @param endOfTimeSchedule the endOfTimeSchedule to set
     */
    public void setEndOfTimeSchedule(Date endOfTimeSchedule) {
	this.endOfTimeSchedule = endOfTimeSchedule;
    }

}
