/**
 * 
 */
package org.y3.jrun.model.disciplinetimeschedule;

import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

/**
 * @author Ryppo
 * 
 */
public class DisciplineTimeScheduleTableModel extends DefaultTableModel {

    private static final long serialVersionUID = 1L;

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public Class getColumnClass(int columnIndex) {
	return DisciplineTimeSchedule.class;
    }

    public DisciplineTimeSchedule[] getModel() {
	Object objects[] = super.dataVector.toArray();
	ArrayList<DisciplineTimeSchedule> timeScheduleItem = new ArrayList<DisciplineTimeSchedule>(0);
	for (Object o : objects) {
	    if (o instanceof DisciplineTimeSchedule) {
		timeScheduleItem.add((DisciplineTimeSchedule) o);
	    }
	}
	if (timeScheduleItem.size() != 0) {
	    return (DisciplineTimeSchedule[]) timeScheduleItem.toArray();
	} else {
	    return null;
	}
    }

}
