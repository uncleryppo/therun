/**
 * 
 */
package org.y3.jrun.model.disciplinetimeschedule;

import org.y3.jrun.model.ModelListModel;

/**
 * @author Ryppo
 * 
 */
public class DisciplineTimeScheduleListModel extends ModelListModel {

    private static final long serialVersionUID = 1L;

    public DisciplineTimeScheduleListModel(DisciplineTimeSchedule[] models) {
	super(models);
    }

    @Override
    public DisciplineTimeSchedule getElementAt(int arg0) {
	return (DisciplineTimeSchedule) super.getElementAt(arg0);
    }

    @Override
    public DisciplineTimeSchedule[] getModel() {
	return (DisciplineTimeSchedule[]) super.getModel();
    }
    
}
