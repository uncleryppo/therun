package org.y3.jrun.model.participation;

import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.y3.jrun.model.Model;

/**
 * Copyright: 2011 - 2020 Organization: IT-Happens.de
 * 
 * @author Christian.Rybotycky
 */
public class Participation extends Model {

	private String participationNumber;
	private final long zeroResultTime = -3600000;
	private long resultTime = zeroResultTime;
	private int contactId;
	private int competitionId;
	private int disciplineId;
	private int teamId;
	private String participantName;
	private int distanceFromHomeInMeter;
	private boolean paymentDone = false;
	private boolean certificationHandedOver = false;
	private boolean noncompetitive = false;
	private boolean canceled = false;
	private boolean notStarted = false;
	private int rank;
	private int ageClassRank;
	private int genderRank;
	private int genderAgeClassRank;
	private String comment;
	private int donationHospizInEuroCent;
	private boolean registeredOnline;

	/**
	 * @return the competitionId
	 */
	public int getCompetitionId() {
		return competitionId;
	}

	public boolean isResultTimeSet() {
		if (getResultTime() != zeroResultTime) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param competitionId the competitionId to set
	 */
	public void setCompetitionId(int competitionId) {
		this.competitionId = competitionId;
	}

	/**
	 * @return the participationNumber
	 */
	public String getParticipationNumber() {
		if (participationNumber == null) {
			participationNumber = "";
		}
		return participationNumber;
	}

	/**
	 * @param participationNumber the participationNumber to set
	 */
	public void setParticipationNumber(String participationNumber) {
		this.participationNumber = participationNumber;
	}

	/**
	 * @return the resultTime
	 */
	public long getResultTime() {
		return resultTime;
	}

	/**
	 * @return the result time
	 */
	public String getResultTimeAsString() {
		return Long.toString(resultTime);
	}

	/**
	 * @return the result time
	 */
	public Date getResultTimeAsDate() {
		return new Date(resultTime);
	}

	/**
	 * @param _resultTime the resultTime to set
	 */
	public void setResultTime(long _resultTime) {
		resultTime = _resultTime;
	}

	/**
	 * @param _resultTime
	 */
	public void setResultTimeAsString(String _resultTime) {
		if (_resultTime != null && _resultTime.length() != 0) {
			try {
				resultTime = Long.parseLong(_resultTime);
			} catch (NumberFormatException ex) {
				resultTime = -3600000;
			}
		} else {
			resultTime = -3600000;
		}
	}

	/**
	 * @param _resultTime
	 */
	public void setResultTimeAsDate(Date _resultTime) {
		if (_resultTime != null) {
			resultTime = _resultTime.getTime();
		} else {
			resultTime = 0;
		}
	}

	/**
	 * @return the contactId
	 */
	public int getContactId() {
		return contactId;
	}

	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	@Override
	public String toString() {
		String toString = "[" + participationNumber + "] ";
		if (participantName != null) {
			toString += participantName;
		}
		return toString;
	}

	public String getParticipantName() {
		return participantName;
	}

	public void setParticipantName(String participantName) {
		this.participantName = participantName;
	}

	public int getDisciplineId() {
		return disciplineId;
	}

	public int getTeamId() {
		return teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public void setDisciplineId(int disciplineId) {
		this.disciplineId = disciplineId;
	}

	public boolean isPaymentDone() {
		return paymentDone;
	}

	public int isPaymentDoneAsInt() {
		if (paymentDone) {
			return 1;
		} else {
			return 0;
		}
	}

	public void setPaymentDone(boolean paymentDone) {
		this.paymentDone = paymentDone;
	}

	public void setPaymentDone(int paymentDone) {
		if (paymentDone == 0) {
			this.paymentDone = false;
		} else if (paymentDone == 1) {
			this.paymentDone = true;
		}
	}

	public boolean isCertificationHandedOver() {
		return certificationHandedOver;
	}

	public void setCertificationHandedOver(boolean certificationHandedOver) {
		this.certificationHandedOver = certificationHandedOver;
	}

	public int isCertificationHandedOverAsInt() {
		if (certificationHandedOver) {
			return 1;
		} else {
			return 0;
		}
	}

	public void setCertificationHandedOver(int certificationHandedOver) {
		if (certificationHandedOver == 0) {
			this.certificationHandedOver = false;
		} else if (certificationHandedOver == 1) {
			this.certificationHandedOver = true;
		}
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public int getAgeClassRank() {
		return ageClassRank;
	}

	public void setAgeClassRank(int _ageClassRank) {
		ageClassRank = _ageClassRank;
	}

	@Override
	public void generateImportFingerPrint() {
		setImportFingerPrint(toString());
	}

	public int isNoncompetitiveAsInt() {
		if (noncompetitive) {
			return 1;
		} else {
			return 0;
		}
	}

	public int isCanceledAsInt() {
		if (canceled) {
			return 1;
		} else {
			return 0;
		}
	}

	public int isNotStartedAsInt() {
		if (notStarted) {
			return 1;
		} else {
			return 0;
		}
	}

	public boolean isNoncompetitive() {
		return noncompetitive;
	}

	public boolean isCanceled() {
		return canceled;
	}

	public boolean isNotStarted() {
		return notStarted;
	}

	public void setNoncompetitive(boolean noncompetitive) {
		this.noncompetitive = noncompetitive;
	}

	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}

	public void setNotStarted(boolean notStarted) {
		this.notStarted = notStarted;
	}

	public void setNoncompetitive(int noncompetitive) {
		if (noncompetitive == 0) {
			this.noncompetitive = false;
		} else if (noncompetitive == 1) {
			this.noncompetitive = true;
		}
	}

	public void setCanceled(int canceled) {
		if (canceled == 0) {
			this.canceled = false;
		} else if (canceled == 1) {
			this.canceled = true;
		}
	}

	public void setNotStarted(int notStarted) {
		if (notStarted == 0) {
			this.notStarted = false;
		} else if (notStarted == 1) {
			this.notStarted = true;
		}
	}

	public int getGenderRank() {
		return genderRank;
	}

	public void setGenderRank(int genderRank) {
		this.genderRank = genderRank;
	}

	public int getGenderAgeClassRank() {
		return genderAgeClassRank;
	}

	public void setGenderAgeClassRank(int genderAgeClassRank) {
		this.genderAgeClassRank = genderAgeClassRank;
	}

	public String getComment() {
		if (comment == null) {
			return "";
		}
		return comment;
	}

	public void setComment(String _comment) {
		comment = _comment;
	}

	public int getDonationHospizInEuroCent() {
		return donationHospizInEuroCent;
	}

	public static int getSummaryDonationInEuroCent(Participation[] participations) {
		if (participations == null || participations.length == 0) {
			return 0;
		} else {
			int sum = 0;
			for (Participation participation : participations) {
				sum += participation.getDonationHospizInEuroCent();
			}
			return sum;
		}
	}

	public static String getSummaryDonationInEuroCentAsString(Participation[] participations) {
		return formatDonationIntToString(getSummaryDonationInEuroCent(participations));
	}

	private static String formatDonationIntToString(int donationInt) {
		return java.text.NumberFormat.getCurrencyInstance(Locale.GERMANY)
				.format(donationInt > 0 ? donationInt / 100 : 0d);
	}

	public String getDonationHospizAsEuroString() {
		return formatDonationIntToString(donationHospizInEuroCent);
	}

	public void setDonationHospizInEuroCent(int _donationHospiz) {
		donationHospizInEuroCent = _donationHospiz;
	}

	public int getDistanceFromHomeInMeter() { return distanceFromHomeInMeter; }

	public String getDistanceFromHomeAsKmString() {
		double dValueInMeter = distanceFromHomeInMeter;
		NumberFormat kmFormat = NumberFormat.getInstance(Locale.GERMANY);
		kmFormat.setMinimumIntegerDigits(1);
		kmFormat.setMaximumIntegerDigits(5);
		return kmFormat.format(dValueInMeter > 0 ? dValueInMeter / 1_000: 0d);
	}

	public void setDistanceFromHomeInMeter(int _distanceFromHomeInMeter) { distanceFromHomeInMeter = _distanceFromHomeInMeter; }

	public boolean isRegisteredOnline() {
		return registeredOnline;
	}

	public void setRegisteredOnline(boolean registeredOnline) {
		this.registeredOnline = registeredOnline;
	}

	public int isRegisteredOnlineAsInt() {
		if (registeredOnline) {
			return 1;
		} else {
			return 0;
		}
	}

	public void setRegisteredOnline(int registeredOnline) {
		if (registeredOnline == 0) {
			this.registeredOnline = false;
		} else if (registeredOnline == 1) {
			this.registeredOnline = true;
		}
	}

}
