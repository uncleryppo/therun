/**
 * 
 */
package org.y3.jrun.model.team;

import java.util.Arrays;

import org.y3.jrun.model.ModelListModel;
import org.y3.jrun.view.model.team.TeamComparator;

/**
 * @author Ryppo
 *
 */
public class TeamListModel extends ModelListModel {
    
    private static final long serialVersionUID = 1L;

    /**
     * @param models
     */
    public TeamListModel(Team[] models) {
	super(models);
    }
    
    @Override
    public Team getElementAt(int arg0) {
	return (Team) super.getElementAt(arg0);
    }
    
    public Team[] getModel() {
	return (Team[]) super.getModel();
    }
    
    public void sort(TeamComparator comparator) {
	Arrays.sort(getModel(), comparator);
    }

}
