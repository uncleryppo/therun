/**
 *
 */
package org.y3.jrun.model.team;

import org.apache.commons.lang.StringUtils;
import org.y3.jrun.model.Model;

/**
 * Represents an organization of people
 * Copyright: 2011 - 2018
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class Team extends Model {

    private String name;

    @Override
    public boolean isNotEmptyHint() {
        return StringUtils.isNotEmpty(name);
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.model.Model#toString()
     */
    @Override
    public String toString() {
        return getName();
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.model.Model#generateImportFingerPrint()
     */
    @Override
    public void generateImportFingerPrint() {
        setImportFingerPrint(toString());
    }

    /**
     * @return the name
     */
    public String getName() {
        if (name == null) {
            return "";
        }
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

}
