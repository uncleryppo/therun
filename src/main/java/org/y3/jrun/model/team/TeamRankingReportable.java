package org.y3.jrun.model.team;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.y3.jrun.control.KeywordsDictionary;
import org.y3.jrun.control.Utils;
import org.y3.jrun.model.participation.RichParticipation;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 * Copyright: 2011 - 2018
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class TeamRankingReportable implements JRDataSource {

	private static Logger log = LogManager.getLogger();
    
    private int cursor = -1;
    private RichParticipation[] richParticipations;
    private HashSet<DataSet> teamCertifications;
    private DataSet[] rankedTeams;
    
    public TeamRankingReportable(RichParticipation[] _richParticipations) {
	richParticipations = _richParticipations;
	createDataset();
	rankedTeams = calculateRanking(teamCertifications);
    }
    
    public void createDataset() {
	teamCertifications = new HashSet<DataSet>();
	if (richParticipations != null && richParticipations.length > 0) {
	    //find all teams
	    String[] teamNames = Utils.getUniqueTeamNames(richParticipations);
	    if (teamNames != null && teamNames.length > 0) {
		//loop the teams to collect certifications
		for (String currentTeam: teamNames) {
		    HashSet<RichParticipation> teamParticipations = new HashSet<RichParticipation>();
		    for (RichParticipation currentParticipation: richParticipations) {
			if (currentParticipation != null && currentParticipation.getTeam() != null && currentParticipation.getTeam().getName() != null && currentParticipation.getTeam().getName().equals(currentTeam)) {
			    teamParticipations.add(currentParticipation);
			}
		    }
		    if (teamParticipations.size() > 0) {
			Iterator<RichParticipation> iter = teamParticipations.iterator();
			DataSet teamCertification = new DataSet(currentTeam, 0, 0, 0, 0);
			while (iter.hasNext()) {
			    RichParticipation part = iter.next();
			    int rank = part.getGenderAgeClassRank();
			    if (rank == 1) {
				teamCertification.countGold++;
			    } else if (rank == 2) {
				teamCertification.countSilver++;
			    } else if (rank == 3) {
				teamCertification.countBronze++;
			    }
			}
			if (teamCertification != null) {
			    teamCertifications.add(teamCertification);
			}
		    }
		}
	    }
	}
    }

    @Override
    public Object getFieldValue(JRField field) throws JRException {
	Object returnValue = null;
	DataSet model = null;
	if (rankedTeams != null) {
	model = rankedTeams[cursor];
	}
	if (model != null && field != null) {
	    log.trace("Cursor (" + cursor + ") RankedTeam (" + model.teamName + ")");
	    if (field.getName().equals(KeywordsDictionary.TEAM_NAME)) {
		returnValue = model.teamName;
	    } else if (field.getName().equals(KeywordsDictionary.RANK_GOLD)) {
		returnValue = Integer.toString(model.countGold);
	    } else if (field.getName().equals(KeywordsDictionary.RANK_SILVER)) {
		returnValue = Integer.toString(model.countSilver);
	    } else if (field.getName().equals(KeywordsDictionary.RANK_BRONZE)) {
		returnValue = Integer.toString(model.countBronze);
	    }
	}
	return returnValue;
    }
    
    public DataSet[] calculateRanking(HashSet<DataSet> unsortedDataSets) {
	DataSet[] sortedDataSets = null;
	if (unsortedDataSets != null) {
	    sortedDataSets = new DataSet[unsortedDataSets.size()];
	    sortedDataSets = unsortedDataSets.toArray(new DataSet[0]);
	    Arrays.sort(sortedDataSets, new DataSetComparator());
	}
	return sortedDataSets;
    }

    @Override
    public boolean next() throws JRException {
	boolean next = false;
	cursor++;
	if (rankedTeams != null && cursor < rankedTeams.length) {
	    next = true;
	}
	return next;
    }

}

class DataSet implements Comparable<DataSet>{

	private static Logger log = LogManager.getLogger();
    
    String teamName = "";
    int countGold = 0;
    int countSilver = 0;
    int countBronze = 0;
    int position;
    
    public DataSet(String _teamName, int _countGold, int _countSilver,  int _countBronze, int _position) {
	teamName = _teamName;
	countGold = _countGold;
	countSilver = _countSilver;
	countBronze = _countBronze;
	position = _position;
    }
    
    public DataSet() {
    }

    
    @Override
    public int compareTo(DataSet o2) {
	if (this == null && o2 == null) {
	    return 0;
	} else if (this == null) {
	    return 1;
	} else if (o2 == null) {
	    return -1;
	} else {
	    log.trace("compare o1: " + this.teamName + " to o2: " + o2.teamName);
	    //gold
	    int goldComparism = compareInt(this.countGold, o2.countGold);
	    if (goldComparism != 0) {
		return goldComparism;
	    }
	    //silver
	    int silverComparism = compareInt(this.countSilver, o2.countSilver);
	    if (silverComparism != 0) {
		return silverComparism;
	    }
	    //bronze
	    return compareInt(this.countBronze, o2.countBronze);
	}
    }
	
	private int compareInt(int i1, int i2) {
		if (i1 == i2) {
		    return 0;
		}
		if (i1 < i2) {
		    return 1;
		}
		else return -1;
	    }
}

class DataSetComparator implements Comparator<DataSet> {

	private static Logger log = LogManager.getLogger();

    @Override
    public int compare(DataSet o1, DataSet o2) {
	int returnValue = -2;
	log.trace("DataSetComparator: o1: " + o1.teamName + " to o2: " + o2.teamName);
	if (o1 == null && o2 == null) {
	    returnValue = 0;
	} else if (o1 == null) {
	    returnValue = 1;
	} else if (o2 == null) {
	    returnValue = -1;
	} else {
	    //gold
	    int goldComparism = compareInt(o1.countGold, o2.countGold);
	    if (goldComparism != 0) {
		returnValue = goldComparism;
	    } else {
	    //silver
		int silverComparism = compareInt(o1.countSilver, o2.countSilver);
		if (silverComparism != 0) {
		    returnValue = silverComparism;
		} else {
		//bronze
		returnValue = compareInt(o1.countBronze, o2.countBronze);
	    	}
	    }
	}
	log.trace(" __ [ " + returnValue + " ] __");
	return returnValue;
    }
    
    private int compareInt(int i1, int i2) {
	log.trace(" {compareInt(" + i1 + "/" + i2 + ")}");
	if (i1 == i2) {
	    return 0;
	}
	if (i1 < i2) {
	    return 1;
	}
	else return -1;
    }
}