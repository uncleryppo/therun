package org.y3.jrun.control;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.y3.jrun.model.ageclass.AgeClass;
import org.y3.jrun.model.competition.Competition;
import org.y3.jrun.model.contact.Contact;
import org.y3.jrun.model.discipline.Discipline;
import org.y3.jrun.model.participation.RichParticipation;

/**
 * Copyright: 2011 - 2019
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class Utils {

	private static Logger log = LogManager.getLogger();

	public static String parseIntegerFromStringAsString(String integerInString) {
		int searchInt = 9999;
		try {
			searchInt = Integer.parseInt(integerInString);
		} catch (NumberFormatException nfe) {
		}
		return Integer.toString(searchInt);
	}
	
	public static int parseIntValueFromStringDefault0(String stringValue) {
		int intValue = 0;
		try {
			if (stringValue != null) {
				intValue = Integer.parseInt(stringValue);
			}
		} catch (NumberFormatException e) {
		}
		return intValue;
	}
	
	public static double parseDoubleValueFromStringDefault0(String stringValue) {
		double doubleValue = 0d;
		try {
			if (stringValue != null) {
				doubleValue = Double.parseDouble(stringValue);
			}
		} catch (NumberFormatException e) {
		}
		return doubleValue;
	}
	
	public static String intToStringWith0AsEmpty(int integer) {
		String value = "";
		if (integer != 0) {
			value = Integer.toString(integer);
		}
		return value;
	}
	
	public static boolean isSearchStringInsideOfRichParticipation(String searchString, RichParticipation richParticipation) {
		boolean result = false;
		if (richParticipation != null) {
			if (searchString == null || searchString.length() == 0) {
				result = true;
			} else {
				//1. collect attribute values to search in
				ArrayList<String> attributeValues = new ArrayList<String>(0);
				AgeClass ageClass = richParticipation.getAgeClassForParticipant();
				if (ageClass != null) {
					attributeValues.add(ageClass.getChangerName());
					attributeValues.add(ageClass.getCreatorName());
					attributeValues.add(ageClass.getImportFingerPrint());
					attributeValues.add(ageClass.getNotificationMessage());
					attributeValues.add(ageClass.getNotificationTitle());
				}
				attributeValues.add(richParticipation.getChangerName());
				attributeValues.add(richParticipation.getCreatorName());
				attributeValues.add(richParticipation.getImportFingerPrint());
				attributeValues.add(richParticipation.getNotificationMessage());
				attributeValues.add(richParticipation.getNotificationTitle());
				attributeValues.add(richParticipation.getParticipantName());
				attributeValues.add(richParticipation.getParticipationNumber());
				Competition competition = richParticipation.getRelatedCompetition();
				if (competition != null) {
					attributeValues.add(competition.getChangerName());
					attributeValues.add(competition.getCreatorName());
					attributeValues.add(competition.getImportFingerPrint());
					attributeValues.add(competition.getNotificationMessage());
					attributeValues.add(competition.getNotificationTitle());
					attributeValues.add(competition.getRegistrationPageToString());
					attributeValues.add(competition.getRegistrationsLogFileToString());
					attributeValues.add(competition.getTitle());
					attributeValues.add(competition.getWebPageToString());
				}
				Contact contact = richParticipation.getParticipant();
				if (contact != null) {
					attributeValues.add(contact.getChangerName());
					attributeValues.add(contact.getCreatorName());
					attributeValues.add(contact.getImportFingerPrint());
					attributeValues.add(contact.getNotificationMessage());
					attributeValues.add(contact.getNotificationTitle());
					attributeValues.add(contact.getAddress1());
					attributeValues.add(contact.getAddress2());
					attributeValues.add(contact.getCity());
					attributeValues.add(contact.getComments());
					attributeValues.add(contact.getEmail());
					attributeValues.add(contact.getFirstname());
					attributeValues.add(contact.getFullName());
					attributeValues.add(contact.getLastname());
					attributeValues.add(contact.getMobilenumber());
					attributeValues.add(contact.getName());
					attributeValues.add(contact.getPhonenumber());
				}
				Discipline discipline = richParticipation.getDiscipline();
				if (discipline != null) {
					attributeValues.add(discipline.getChangerName());
					attributeValues.add(discipline.getCreatorName());
					attributeValues.add(discipline.getImportFingerPrint());
					attributeValues.add(discipline.getLength());
					attributeValues.add(discipline.getName());
					attributeValues.add(discipline.getNotificationMessage());
					attributeValues.add(discipline.getNotificationTitle());
				}
				int attributes = attributeValues.size();
				for (int aNo = 0; aNo < attributes; aNo++) {
					String attribute = attributeValues.get(aNo);
					if (attribute != null && StringUtils.contains(StringUtils.upperCase(attribute), StringUtils.upperCase(searchString))) {
						result = true;
						break;
					}
				}
			}
		}
		return result;
	}
	
	public static String[] getUniqueTeamNames(RichParticipation[] richParticipations) {
	    log.debug("BEGIN: Unique team names list");
	    String[] returnValue = null;
	    //create empty team names bag
	    HashSet<String> uniqueTeamNames = null; 
	    if (richParticipations != null && richParticipations.length > 0) {
		uniqueTeamNames = new HashSet<String>();
		for (RichParticipation currentRichParticipation: richParticipations) {
		    if (currentRichParticipation != null && currentRichParticipation.getTeam() != null) {
			String currentTeamName = currentRichParticipation.getTeam().getName();
			Iterator<String> iter = uniqueTeamNames.iterator();
			boolean found = false;
			while (iter.hasNext() && !found) {
			    String foundTeamName = iter.next();
			    if (foundTeamName != null && foundTeamName.equals(currentTeamName)) {
				found = true;
			    }
			}
			if (!found) {
			    uniqueTeamNames.add(currentTeamName);
			}
		    }
		}
	    }
	    //return collected team names
	    if (uniqueTeamNames != null && uniqueTeamNames.size() > 0) {
		returnValue = new String[uniqueTeamNames.size()];
		Iterator<String> iter = uniqueTeamNames.iterator();
		int position = 0;
		while (iter.hasNext()) {
		    returnValue[position] = iter.next();
		    log.debug("[" + position + "] " + returnValue[position]);
		    position++;
		}
	    }
	    log.debug("END: Unique team names list.");
	    return returnValue;
	}
	
}
