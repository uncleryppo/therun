package org.y3.jrun.storage.webrequest;

import net.sf.json.JSONObject;
import org.apache.derby.impl.store.access.sort.Scan;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

/**
 * Copyright: 2022
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public class MapQuestApi {
//WAR ZUM AUSPROBIEREN. UNWICHTIG. GOOGLE-MAPS FUNZT BESSER
    //Probleme hier: 1. Umlaute in Ortsnamen. 2. Findet kleine Orte nicht, wie Othfresen
    HttpURLConnection connection = null;

    private static String mapQuestApi = "http://www.mapquestapi.com/";

    private static String fromToApi = "directions/v2/route?unit=k&routeType=shortest&key=";
    private static String fromApi = "&from=";
    private static String toApi = "&to=";

    public static Double requestDistance(String key, String fromCity, String toCity) {
        String tempFromCity = fromCity.replace(" ","+");
        String tempToCity = toCity.replace(" ","+");
        String requestUrl = mapQuestApi + fromToApi + key + fromApi + tempFromCity + toApi + tempToCity;

        String responseBody = sendRequest(requestUrl);
        //Km rausparsen
        JSONObject jsonResponse = JSONObject.fromObject(responseBody);
        Double distance = jsonResponse.getJSONObject("route").getDouble("distance");
        return distance;
    }

    private static String sendRequest(String requestUrl) {
        System.out.println("Request-URL: " + requestUrl);

        URLConnection connection = null;
        String responseBody = null;

        try {
            String charset = "UTF-8";
            connection = new URL(requestUrl).openConnection();
            connection.setRequestProperty("Accept-Charset",charset);

            InputStream response = connection.getInputStream();

            try (Scanner scanner = new Scanner(response)) {
                responseBody = scanner.useDelimiter("\\A").next();

               //System.out.println("Response:\n" + responseBody+"\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return responseBody;
        }
    }

}
