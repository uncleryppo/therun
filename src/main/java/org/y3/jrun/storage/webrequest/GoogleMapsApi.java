package org.y3.jrun.storage.webrequest;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.Distance;
import com.google.maps.model.DistanceMatrix;

import java.io.IOException;

/**
 * Copyright: 2022
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public class GoogleMapsApi {

    public static Double requestDistance(String key, String fromCity, String toCity) {
        Double distance = null;
        try {
            GeoApiContext context = new GeoApiContext.Builder().apiKey(key).build();
            DistanceMatrix distanceMatrix =  DistanceMatrixApi.getDistanceMatrix(
                    context, new String[]{fromCity}, new String[]{toCity}).await();
            Distance responseDistance = distanceMatrix.rows[0].elements[0].distance;
            distance = ((double) responseDistance.inMeters) / 1000;
        } catch (ApiException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return distance;
    }
}
