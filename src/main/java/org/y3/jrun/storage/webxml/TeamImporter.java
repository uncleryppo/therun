package org.y3.jrun.storage.webxml;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.y3.jrun.control.KeywordsDictionary;
import org.y3.jrun.model.Model;
import org.y3.jrun.model.competition.RichCompetition;
import org.y3.jrun.model.team.Team;

/**
 * Copyright: 2011 - 2018
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class TeamImporter extends ModelImporter {

	private static Logger log = LogManager.getLogger();

    @Override
    protected void fillModelByType(String fieldType, String fieldValue,
	    Model model, RichCompetition sourceCompetition) {
	if (model != null && model instanceof Team && fieldType != null) {
	    Team team = (Team) model;
	    //team name
	    if (fieldType.equals(KeywordsDictionary.REGISTRATIONS_XML_TEAM_NAME)) {
		log.debug("Team-Name: " + fieldValue);
		if (fieldValue != null && !fieldValue.equals("null") && fieldValue.length() > 0 && !fieldValue.equals("false")) {
		    team.setName(StringUtils.left(fieldType, 50));
		}
	    }
	}
    }

    @Override
    public Team getNewModel() {
	return new Team();
    }

    @Override
    public Team[] getNewModelArray(int size) {
	return new Team[size];
    }

}
