package org.y3.jrun.storage.file;

import net.sf.json.JSONObject;

import org.y3.jrun.control.KeywordsDictionary;
import org.y3.jrun.model.Model;
import org.y3.jrun.model.database.DatabaseInformation;

/**
 * Title: 		org.y3.jrun.storage.file - DatabaseInformationExchanger
 * Description:	
 * @author 		rybotycky
 * @version 	$Id$
 */
public class DatabaseInformationExchanger extends ModelExchanger {

	/* 
	 * @see org.y3.jrun.storage.file.ModelExchanger#getModelClassName()
	 */
	@Override
	public String getModelClassName() {
		return KeywordsDictionary.DATABASEINFORMATION;
	}

	/* 
	 * @see org.y3.jrun.storage.file.ModelExchanger#modelToJSON(org.y3.jrun.model.Model)
	 */
	@Override
	public JSONObject modelToJSON(Model _model) {
		JSONObject json = null;
		if (_model != null && _model instanceof DatabaseInformation) {
			DatabaseInformation model = (DatabaseInformation) _model;
			json = new JSONObject();
			json.put(KeywordsDictionary.DATABASEINFORMATION_HOMELOCATION, model.getHomeLocation());
		}
		return json;
	}

	/* 
	 * @see org.y3.jrun.storage.file.ModelExchanger#JSONToModel(net.sf.json.JSONObject)
	 */
	@Override
	public Model JSONToModel(JSONObject json) {
		DatabaseInformation _model = null;
		if (json != null) {
			_model = new DatabaseInformation();
			_model.setHomeLocation(json.getString(KeywordsDictionary.DATABASEINFORMATION_HOMELOCATION));
		}
		return _model;
	}

}
