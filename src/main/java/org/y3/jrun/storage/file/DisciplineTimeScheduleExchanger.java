package org.y3.jrun.storage.file;

import net.sf.json.JSONObject;

import org.y3.jrun.control.KeywordsDictionary;
import org.y3.jrun.model.Model;
import org.y3.jrun.model.disciplinetimeschedule.DisciplineTimeSchedule;

/**
 * @author Ryppo
 *
 */
public class DisciplineTimeScheduleExchanger extends ModelExchanger {

    @Override
    public String getModelClassName() {
	return KeywordsDictionary.DISCIPLINETIMESCHEDULE;
    }

    @Override
    public JSONObject modelToJSON(Model _model) {
	JSONObject json = null;
	if (_model != null && _model instanceof DisciplineTimeSchedule) {
	    DisciplineTimeSchedule model = (DisciplineTimeSchedule) _model;
	    json = new JSONObject();
	    json.put(KeywordsDictionary.MODEL_ID, model.getId());
	    json.put(KeywordsDictionary.DISCIPLINETIMESCHEDULE_RELATED_DISCIPLINE_ID, model.getRelatedDisciplineId());
	    json.put(KeywordsDictionary.DISCIPLINETIMESCHEDULE_START_OF_TIMESCHEDULE, model.getStartOfTimeSchedule());
	    json.put(KeywordsDictionary.DISCIPLINETIMESCHEDULE_END_OF_TIMESCHEDULE, model.getEndOfTimeSchedule());
	    json.put(KeywordsDictionary.DISCIPLINETIMESCHEDULE_BELONGS_TO_TIMESCHEDULE, model.getRelatedTimeScheduleId());
	}
	return json;
    }

    @Override
    public Model JSONToModel(JSONObject json) {
	DisciplineTimeSchedule disciplineTimeSchedule = null;
	if (json != null) {
	    disciplineTimeSchedule = new DisciplineTimeSchedule();
	    disciplineTimeSchedule.setRelatedDisciplineId(json.getInt(KeywordsDictionary.DISCIPLINETIMESCHEDULE_RELATED_DISCIPLINE_ID));
	    disciplineTimeSchedule.setRelatedTimeScheduleId(json.getInt(KeywordsDictionary.DISCIPLINETIMESCHEDULE_BELONGS_TO_TIMESCHEDULE));
//	    disciplineTimeSchedule.setStartOfTimeSchedule(json.get(KeywordsDictionary.DISCIPLINETIMESCHEDULE_START_OF_TIMESCHEDULE));
//	    disciplineTimeSchedule.setEndOfTimeSchedule(json.get(KeywordsDictionary.DISCIPLINETIMESCHEDULE_END_OF_TIMESCHEDULE));
	}
	return disciplineTimeSchedule;
    }

}
