package org.y3.jrun.storage.file;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.y3.jrun.control.KeywordsDictionary;
import org.y3.jrun.model.Model;
import org.y3.jrun.model.ageclass.AgeClass;
import org.y3.jrun.model.disciplinetimeschedule.DisciplineTimeSchedule;
import org.y3.jrun.model.timeschedule.RichTimeSchedule;
import org.y3.jrun.model.timeschedule.TimeSchedule;

/**
 * @author Ryppo
 *
 */
public class TimeScheduleExchanger extends ModelExchanger {

    @Override
    public String getModelClassName() {
	return KeywordsDictionary.TIMESCHEDULE;
    }

    @Override
    public JSONObject modelToJSON(Model model) {
	JSONObject json = null;
	if (model != null && model instanceof RichTimeSchedule) {
	    RichTimeSchedule richTimeSchedule = (RichTimeSchedule) model;
		json = new JSONObject();
		json.put(KeywordsDictionary.MODEL_ID, richTimeSchedule.getId());
		json.put(KeywordsDictionary.TIMESCHEDULE_NAME, richTimeSchedule.getName());
		json.put(KeywordsDictionary.TIMESCHEDULE_NOTE, richTimeSchedule.getNote());
		DisciplineTimeSchedule[] disciplineTimeSchedules = richTimeSchedule.getDisciplineTimeSchedules();
		if (disciplineTimeSchedules != null && disciplineTimeSchedules.length > 0) {
		    DisciplineTimeScheduleExchanger dtse = new DisciplineTimeScheduleExchanger();
		    JSONArray disciplineTimeSchedulesJSON = new JSONArray();
		    for (DisciplineTimeSchedule disciplineTimeSchedule: disciplineTimeSchedules) {
			JSONObject disciplineTimeScheduleJSON = dtse.modelToJSON(disciplineTimeSchedule);
			disciplineTimeSchedulesJSON.add(disciplineTimeScheduleJSON);
		    }
		}
	}
	return json;
    }

    @Override
    public Model JSONToModel(JSONObject json) {
	RichTimeSchedule richTimeSchedule = null;
	if (json != null) {
	    TimeSchedule timeSchedule = new TimeSchedule();
	    timeSchedule.setName(json.getString(KeywordsDictionary.TIMESCHEDULE_NAME));
	    timeSchedule.setNote(json.getString(KeywordsDictionary.TIMESCHEDULE_NOTE));
	    //Discipline time schedules
	    DisciplineTimeSchedule[] disciplineTimeSchedules = null;
	    JSONArray disciplineTimeSchedulesJSON = json.getJSONArray(KeywordsDictionary.DISCIPLINETIMESCHEDULE);
		if (disciplineTimeSchedulesJSON != null) {
		    DisciplineTimeScheduleExchanger dtse = new DisciplineTimeScheduleExchanger();
			int countDisciplineTimeSchedules = disciplineTimeSchedulesJSON.size();
			disciplineTimeSchedules = new DisciplineTimeSchedule[countDisciplineTimeSchedules];
			for (int no = 0; no < countDisciplineTimeSchedules; no++) {
				Object o = disciplineTimeSchedulesJSON.get(no);
				if (o instanceof JSONObject) {
					Model model = dtse.JSONToModel((JSONObject) o);
					if (model instanceof AgeClass) {
					    disciplineTimeSchedules[no] = (DisciplineTimeSchedule) model;
					}
				}
			}
		}
		richTimeSchedule = new RichTimeSchedule(timeSchedule, disciplineTimeSchedules);
	}
	return richTimeSchedule;
    }

}
