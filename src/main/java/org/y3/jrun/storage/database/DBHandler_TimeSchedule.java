package org.y3.jrun.storage.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.y3.jrun.control.KeywordsDictionary;
import org.y3.jrun.model.Model;
import org.y3.jrun.model.timeschedule.TimeSchedule;

/**
 * @author Ryppo
 * 
 */
public class DBHandler_TimeSchedule extends DBModelHandler {

    @Override
    public String getSqlToCreateModelTable() {
	return KeywordsDictionary.SQL_CREATE_TABLE
		+ KeywordsDictionary.DATABASE_SCHEME + "."
		+ KeywordsDictionary.TIMESCHEDULE + " ("
		+ KeywordsDictionary.SQL_ID_INTEGER_PRIMARY_KEY_NOT_NULL + ","
		+ KeywordsDictionary.TIMESCHEDULE_NAME
		+ KeywordsDictionary.SQL_VARCHAR_100 + ","
		+ KeywordsDictionary.TIMESCHEDULE_NOTE
		+ KeywordsDictionary.SQL_VARCHAR_500 + ","
		+ KeywordsDictionary.MODEL_META_ATTRIBUTES_DEFINITION + ")";
    }

    @Override
    public String getSqlToDropModelTable() {
	return KeywordsDictionary.SQL_DROP_TABLE
		+ KeywordsDictionary.DATABASE_SCHEME + "."
		+ KeywordsDictionary.TIMESCHEDULE;
    }

    @Override
    public String getSqlToInsertModel(Model model) {
	if (model == null || !(model instanceof TimeSchedule)) {
	    return null;
	}
	TimeSchedule timeSchedule = (TimeSchedule) model;
	return KeywordsDictionary.SQL_INSERT_INTO
		+ KeywordsDictionary.DATABASE_SCHEME + "."
		+ KeywordsDictionary.TIMESCHEDULE + " ("
		+ KeywordsDictionary.TIMESCHEDULE_NAME + ", "
		+ KeywordsDictionary.TIMESCHEDULE_NOTE + ", "
		+ KeywordsDictionary.MODEL_META_ATTRIBUTES
		+ KeywordsDictionary.SQL_VALUES + "'" + timeSchedule.getName()
		+ "','" + timeSchedule.getNote() + "','"
		+ KeywordsDictionary.getMODEL_META_ATTRIBUTE_VALUES(model)
		+ ")";
    }

    @Override
    public String getSqlToUpdateModel(Model model) {
	if (model == null || !(model instanceof TimeSchedule)) {
	    return null;
	}
	TimeSchedule timeSchedule = (TimeSchedule) model;
	return KeywordsDictionary.SQL_UPDATE
		+ KeywordsDictionary.DATABASE_SCHEME + "."
		+ KeywordsDictionary.TIMESCHEDULE + KeywordsDictionary.SQL_SET
		+ KeywordsDictionary.TIMESCHEDULE_NAME + "='"
		+ timeSchedule.getName() + "', "
		+ KeywordsDictionary.TIMESCHEDULE_NOTE + "='"
		+ timeSchedule.getNote() + "', "
		+ KeywordsDictionary.getMODEL_META_ATTRIBUTES_FILLED(model)
		+ KeywordsDictionary.SQL_WHERE_ID_IS + timeSchedule.getId();
    }

    @Override
    public String getSqlForFullTextSearch(String searchString) {
	String whereClause = KeywordsDictionary.SQL_UPPER(KeywordsDictionary.TIMESCHEDULE_NAME) + KeywordsDictionary.SQL_LIKE_UPPER(searchString)
		+ KeywordsDictionary.SQL_OR + KeywordsDictionary.SQL_UPPER(KeywordsDictionary.TIMESCHEDULE_NOTE + KeywordsDictionary.SQL_LIKE_UPPER(searchString));
	return getSqlToLoadModels(whereClause);
    }

    @Override
    public String getSqlToLoadAllModels() {
	return KeywordsDictionary.SQL_SELECT + KeywordsDictionary.MODEL_ID
		+ ", " + KeywordsDictionary.TIMESCHEDULE_NAME
		+ ", " + KeywordsDictionary.TIMESCHEDULE_NOTE
		+ ", "
		+ KeywordsDictionary.MODEL_META_ATTRIBUTES
		+ KeywordsDictionary.SQL_FROM
		+ KeywordsDictionary.DATABASE_SCHEME + "."
		+ KeywordsDictionary.TIMESCHEDULE;
    }

    @Override
    public TimeSchedule[] mapResultSetToModels(ResultSet resultSet)
	    throws SQLException {
	TimeSchedule[] timeSchedule = null;
	if (resultSet != null) {
	    // create array
	    resultSet.last();
	    timeSchedule = new TimeSchedule[resultSet.getRow()];
	    resultSet.beforeFirst();
	    // loop resultSet
	    int timeScheduleCount = 0;
	    while (resultSet.next()) {
		TimeSchedule ts = new TimeSchedule();
		ts.setId(resultSet.getInt(1));
		ts.setName(resultSet.getString(2));
		ts.setNote(resultSet.getString(3));
		KeywordsDictionary.setMODEL_META_ATTRIBUTES(ts, resultSet, 4);
		timeSchedule[timeScheduleCount] = ts;
		timeScheduleCount++;
	    }
	}
	return timeSchedule;
    }

}
