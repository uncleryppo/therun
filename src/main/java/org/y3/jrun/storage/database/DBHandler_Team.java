/**
 * 
 */
package org.y3.jrun.storage.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.y3.jrun.control.KeywordsDictionary;
import org.y3.jrun.model.Model;
import org.y3.jrun.model.team.Team;

/**
 * Copyright: 2011 - 2018
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class DBHandler_Team extends DBModelHandler {

    /* (non-Javadoc)
     * @see org.y3.jrun.storage.database.DBModelHandler#getSqlToCreateModelTable()
     */
    @Override
    public String getSqlToCreateModelTable() {
	return KeywordsDictionary.SQL_CREATE_TABLE
		+ KeywordsDictionary.DATABASE_SCHEME
		+ "." + KeywordsDictionary.TEAM + " ("
		+ KeywordsDictionary.SQL_ID_INTEGER_PRIMARY_KEY_NOT_NULL + ","
		+ KeywordsDictionary.TEAM_NAME + KeywordsDictionary.SQL_VARCHAR_100 + ","
		+ KeywordsDictionary.MODEL_META_ATTRIBUTES_DEFINITION
		+ ")";
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.storage.database.DBModelHandler#getSqlToDropModelTable()
     */
    @Override
    public String getSqlToDropModelTable() {
	return KeywordsDictionary.SQL_DROP_TABLE
		+ KeywordsDictionary.DATABASE_SCHEME
		+ "." + KeywordsDictionary.TEAM;
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.storage.database.DBModelHandler#getSqlToInsertModel(org.y3.jrun.model.Model)
     */
    @Override
    public String getSqlToInsertModel(Model model) {
	if (model == null || !(model instanceof Team)) {
	    return null;
	}
	Team team = (Team) model;
	return KeywordsDictionary.SQL_INSERT_INTO
		+ KeywordsDictionary.DATABASE_SCHEME
		+ "." + KeywordsDictionary.TEAM + " ("
		+ KeywordsDictionary.TEAM_NAME + ", "
		+ KeywordsDictionary.MODEL_META_ATTRIBUTES
		+ KeywordsDictionary.SQL_VALUES
		+ "'" + team.getName() + "','"
		+ KeywordsDictionary.getMODEL_META_ATTRIBUTE_VALUES(team)
		+ ")";
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.storage.database.DBModelHandler#getSqlToUpdateModel(org.y3.jrun.model.Model)
     */
    @Override
    public String getSqlToUpdateModel(Model model) {
	if (model == null || !(model instanceof Team)) {
	    return null;
	}
	Team team = (Team) model;
	return KeywordsDictionary.SQL_UPDATE + KeywordsDictionary.DATABASE_SCHEME + "."
		+ KeywordsDictionary.TEAM + KeywordsDictionary.SQL_SET
		+ KeywordsDictionary.TEAM_NAME + "= '"
		+ team.getName() + "' "
		+ KeywordsDictionary.SQL_WHERE_ID_IS
		+ model.getId();
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.storage.database.DBModelHandler#getSqlForFullTextSearch(java.lang.String)
     */
    @Override
    public String getSqlForFullTextSearch(String searchString) {
	String whereClause =
		KeywordsDictionary.TEAM_NAME + "='" + searchString + "'";
	return getSqlToLoadModels(whereClause);
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.storage.database.DBModelHandler#getSqlToLoadAllModels()
     */
    @Override
    public String getSqlToLoadAllModels() {
	return KeywordsDictionary.SQL_SELECT
		+ KeywordsDictionary.MODEL_ID + ", "
		+ KeywordsDictionary.TEAM_NAME + ", "
		+ KeywordsDictionary.MODEL_META_ATTRIBUTES
		+ KeywordsDictionary.SQL_FROM
		+ KeywordsDictionary.DATABASE_SCHEME + "."
		+ KeywordsDictionary.TEAM;
    }

    /* (non-Javadoc)
     * @see org.y3.jrun.storage.database.DBModelHandler#mapResultSetToModels(java.sql.ResultSet)
     */
    @Override
    public Team[] mapResultSetToModels(ResultSet resultSet)
	    throws SQLException {
	Team[] teams = null;
	if (resultSet != null) {
	    //create array
	    resultSet.last();
	    teams = new Team[resultSet.getRow()];
	    resultSet.beforeFirst();
	    //loop resultSet
	    int teamsCount = 0;
	    while (resultSet.next()) {
		Team t = new Team();
		t.setId(resultSet.getInt(1));
		t.setName(resultSet.getString(2));
		KeywordsDictionary.setMODEL_META_ATTRIBUTES(t, resultSet, 3);
		teams[teamsCount] = t;
		teamsCount++;
	    }
	}
	return teams;
    }

}
