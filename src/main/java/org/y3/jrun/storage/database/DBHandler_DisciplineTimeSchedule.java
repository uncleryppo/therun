package org.y3.jrun.storage.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.y3.jrun.control.KeywordsDictionary;
import org.y3.jrun.control.Utils;
import org.y3.jrun.model.Model;
import org.y3.jrun.model.disciplinetimeschedule.DisciplineTimeSchedule;

/**
 * @author Ryppo
 *
 */
public class DBHandler_DisciplineTimeSchedule extends DBModelHandler {

    @Override
    public String getSqlToCreateModelTable() {
	return KeywordsDictionary.SQL_CREATE_TABLE
		+ KeywordsDictionary.DATABASE_SCHEME + "."
		+ KeywordsDictionary.DISCIPLINETIMESCHEDULE + " ("
		+ KeywordsDictionary.SQL_ID_INTEGER_PRIMARY_KEY_NOT_NULL + ","
		+ KeywordsDictionary.DISCIPLINETIMESCHEDULE_BELONGS_TO_TIMESCHEDULE
		+ KeywordsDictionary.SQL_INTEGER + ","
		+ KeywordsDictionary.DISCIPLINETIMESCHEDULE_RELATED_DISCIPLINE_ID
		+ KeywordsDictionary.SQL_INTEGER + ","
		+ KeywordsDictionary.DISCIPLINETIMESCHEDULE_START_OF_TIMESCHEDULE + KeywordsDictionary.SQL_TIMESTAMP + ","
		+ KeywordsDictionary.DISCIPLINETIMESCHEDULE_END_OF_TIMESCHEDULE + KeywordsDictionary.SQL_TIMESTAMP + ","
		+ KeywordsDictionary.MODEL_META_ATTRIBUTES_DEFINITION 
		+ ")";
    }

    @Override
    public String getSqlToDropModelTable() {
	return KeywordsDictionary.SQL_DROP_TABLE
		+ KeywordsDictionary.DATABASE_SCHEME + "."
		+ KeywordsDictionary.DISCIPLINETIMESCHEDULE;
    }

    @Override
    public String getSqlToInsertModel(Model model) {
	if (model == null || !(model instanceof DisciplineTimeSchedule)) {
		return null;
	}
	DisciplineTimeSchedule disciplineTimeSchedule = (DisciplineTimeSchedule) model;
	return KeywordsDictionary.SQL_INSERT_INTO
			+ KeywordsDictionary.DATABASE_SCHEME + "." 
	+ KeywordsDictionary.DISCIPLINETIMESCHEDULE + " ("
			+ KeywordsDictionary.DISCIPLINETIMESCHEDULE_BELONGS_TO_TIMESCHEDULE + ", "
			+ KeywordsDictionary.DISCIPLINETIMESCHEDULE_RELATED_DISCIPLINE_ID + ", "
			+ KeywordsDictionary.DISCIPLINETIMESCHEDULE_START_OF_TIMESCHEDULE + ", "
			+ KeywordsDictionary.DISCIPLINETIMESCHEDULE_END_OF_TIMESCHEDULE + ", "
			+ KeywordsDictionary.MODEL_META_ATTRIBUTES
			+ KeywordsDictionary.SQL_VALUES 
			+ disciplineTimeSchedule.getRelatedTimeScheduleId() + ", "
			+ disciplineTimeSchedule.getRelatedDisciplineId() + ", '"
			+ DatabaseSession.dateToTimestampString(disciplineTimeSchedule.getStartOfTimeSchedule()) + "', '"
			+ DatabaseSession.dateToTimestampString(disciplineTimeSchedule.getEndOfTimeSchedule()) + "', '"
			+ KeywordsDictionary.getMODEL_META_ATTRIBUTE_VALUES(model)
			+ ")";
    }

    @Override
    public String getSqlToUpdateModel(Model model) {
	if (model == null || !(model instanceof DisciplineTimeSchedule)) {
		return null;
	}
	DisciplineTimeSchedule disciplineTimeSchedule = (DisciplineTimeSchedule) model;
	return KeywordsDictionary.SQL_UPDATE
			+ KeywordsDictionary.DATABASE_SCHEME + ".DISCIPLINETIMESCHEDULE SET "
			+ KeywordsDictionary.DISCIPLINETIMESCHEDULE_BELONGS_TO_TIMESCHEDULE 	+ "=" + disciplineTimeSchedule.getRelatedTimeScheduleId() + ", "
			+ KeywordsDictionary.DISCIPLINETIMESCHEDULE_RELATED_DISCIPLINE_ID 	+ "=" + disciplineTimeSchedule.getRelatedDisciplineId() + ", "
			+ KeywordsDictionary.DISCIPLINETIMESCHEDULE_START_OF_TIMESCHEDULE 	+ "='" + DatabaseSession.dateToTimestampString(disciplineTimeSchedule.getStartOfTimeSchedule())	+ "', "
			+ KeywordsDictionary.DISCIPLINETIMESCHEDULE_END_OF_TIMESCHEDULE 	+ "='" + DatabaseSession.dateToTimestampString(disciplineTimeSchedule.getEndOfTimeSchedule())	+ "', "
			+ KeywordsDictionary.getMODEL_META_ATTRIBUTES_FILLED(model)
			+ KeywordsDictionary.SQL_WHERE_ID_IS 					+ disciplineTimeSchedule.getId();
    }

    @Override
    public String getSqlForFullTextSearch(String searchString) {
	String searchIntegerString = Utils.parseIntegerFromStringAsString(searchString);
	String whereClause =
			KeywordsDictionary.DISCIPLINETIMESCHEDULE_BELONGS_TO_TIMESCHEDULE + searchIntegerString + KeywordsDictionary.SQL_OR
			+ KeywordsDictionary.SQL_UPPER(KeywordsDictionary.DISCIPLINETIMESCHEDULE_RELATED_DISCIPLINE_ID) + searchIntegerString;
	return getSqlToLoadModels(whereClause);
    }

    @Override
    public String getSqlToLoadAllModels() {
	return KeywordsDictionary.SQL_SELECT
		+ KeywordsDictionary.MODEL_ID + ", "
		+ KeywordsDictionary.DISCIPLINETIMESCHEDULE_BELONGS_TO_TIMESCHEDULE + ", "
		+ KeywordsDictionary.DISCIPLINETIMESCHEDULE_RELATED_DISCIPLINE_ID + ", "
		+ KeywordsDictionary.DISCIPLINETIMESCHEDULE_START_OF_TIMESCHEDULE + ", "
		+ KeywordsDictionary.DISCIPLINETIMESCHEDULE_END_OF_TIMESCHEDULE + ", "
		+ KeywordsDictionary.MODEL_META_ATTRIBUTES
		+ KeywordsDictionary.SQL_FROM
		+ KeywordsDictionary.DATABASE_SCHEME + "." + KeywordsDictionary.DISCIPLINETIMESCHEDULE;
    }

    @Override
    public DisciplineTimeSchedule[] mapResultSetToModels(ResultSet resultSet)
	    throws SQLException {
	DisciplineTimeSchedule[] disciplineTimeSchedules = null;
	if (resultSet != null) {
		//create array
		resultSet.last();
		disciplineTimeSchedules = new DisciplineTimeSchedule[resultSet.getRow()];
		resultSet.beforeFirst();
		//loop resultSet
		int disciplineTimeSchedulesCount = 0;
		while (resultSet.next()) {
		    DisciplineTimeSchedule d = new DisciplineTimeSchedule();
			d.setId(resultSet.getInt(1));
			d.setRelatedTimeScheduleId(resultSet.getInt(2));
			d.setRelatedDisciplineId(resultSet.getInt(3));
			d.setStartOfTimeSchedule(resultSet.getTimestamp(4));
			d.setEndOfTimeSchedule(resultSet.getTimestamp(5));
			KeywordsDictionary.setMODEL_META_ATTRIBUTES(d, resultSet, 6);
			disciplineTimeSchedules[disciplineTimeSchedulesCount] = d;
			disciplineTimeSchedulesCount++;
		}
	}
	return disciplineTimeSchedules;
    }

}
