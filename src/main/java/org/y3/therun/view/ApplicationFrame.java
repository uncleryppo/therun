package org.y3.therun.view;

import javax.swing.JFrame;

import org.y3.therun.control.ModelController;

/**
 * Copyright: 2015 - 2018
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class ApplicationFrame extends JFrame {
    
    private ModelController modelController;
    
    public ApplicationFrame(ModelController _modelController) {
        modelController = _modelController;
    }

}
