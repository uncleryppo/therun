package org.y3.therun.model.ageclass;

import org.y3.commons.model.IModel_model;

/**
 * Copyright: 2015 - 2018
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class AgeClass_model implements IModel_model {
    
    private Integer id;
    private String title;
    private Integer yearFrom;
    private Integer yearTo;
	private String shortName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getYearFrom() {
        return yearFrom;
    }

    public void setYearFrom(Integer yearFrom) {
        this.yearFrom = yearFrom;
    }

    public Integer getYearTo() {
        return yearTo;
    }

    public void setYearTo(Integer yearTo) {
        this.yearTo = yearTo;
    }

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

}
