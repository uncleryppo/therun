package org.y3.therun.model.ageclass;

import org.y3.commons.model.IModel_list;

/**
 * Copyright: 2015 - 2018
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class AgeClass_list extends IModel_list{

    @Override
    public AgeClass_model getModel(int position) {
        return (AgeClass_model) get(position);
    }

}
